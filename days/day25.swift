#!/usr/bin/env swift

import Foundation

struct GrayChanges: Sequence, IteratorProtocol {
    // Given a number of bits, this will return a series of changes to the bit
    // pattern in the form:
    // (bit number, true/false)
    // As part of the initialisation, we specify if we're starting at all 1's,
    // or 0's

    // This isn't necessarily the optimal way of doing it, but I couldn't find
    // a better algorithm while surfing the Information Superhighway :-(
    // Note that the lowest bit is on the left!
    // (For a 3-bit gray code)
    // Limits:    1  2  4
    // Count:     0  0  0
    // Switching: T  T  T
    // Bits:      1  1  1
    //
    // In each iteration we increment the count on each bit.
    // If the count reaches the limit{
    //     if switching is true {
    //         we flip the bit
    //     }
    //     We flip switching
    // }
    // Only one bit will have been flipped - return that one.

    var grayLimits: [Int]
    var grayCount: [Int]
    var graySwitching: [Bool]
    var grayBits: [Bool]

    init(bits: Int, state: Bool) {
        // bits: The number of bits we're handling
        // state: Are all the bits true, or false to start with?
        grayLimits = [1]
        for bit in 1..<bits {
            grayLimits.append(grayLimits[bit - 1]*2)
        }
        grayBits = Array(repeating: state, count: bits)
        graySwitching = Array(repeating: true, count: bits)
        grayCount = Array(repeating: 0, count: bits)
    }

    mutating func next() -> (Int, Bool)? {
        var change: (Int, Bool)?

        for count in 0..<grayBits.count {
            grayCount[count] += 1
            if grayCount[count] >= grayLimits[count] {
                // Time to switch the bit if switching
                if graySwitching[count] {
                    grayBits[count] = !grayBits[count]
                    change = (count, grayBits[count])
                }
                graySwitching[count] = !graySwitching[count]
                grayCount[count] = 0
            }
        }
        return change
    }
}

// For day 25 we need a couple of small changes:
// For input, we ask on the command line
// All outputs are sent directly to the command line
class IntCode {
    var halted: Bool
    var memory: [Int: Int] = [:]
    var input: [Int]
    var output: [Int]
    var progCounter: Int
    var relBase: Int
    var starved: Bool
    init(_ code: [Int], input: [Int]? = nil) {
        self.halted = false // We halt when we hit 99
        if let inp = input {
            self.input = inp
        } else {
            self.input = []
        }
        self.starved = false
        self.progCounter = 0
        self.relBase = 0
        self.output = []
        // Put the code into memory
        for ind in code.indices {
            self.memory[ind] = code[ind]
        }
    }
    func run(_ input: [Int]) -> [Int]? {
        if self.halted {
            print("Halted")
            return nil
        }
        self.input += input
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            var codestr = String(self.memory[self.progCounter, default: 0])
            let opstr = codestr.suffix(2)
            let opcode = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.progCounter += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            for i in 0..<3 {
                var paramType = 0
                if codestr.count > 0 {
                    paramType = Int(String(codestr.popLast() ?? "0"))!
                }
                switch paramType {
                case 0:
                    // Positional
                    paramPos.append(self.memory[self.progCounter + i, default: 0])
                case 1:
                    // Immediate
                    paramPos.append(self.progCounter + i)
                case 2:
                    // Relative
                    // ie relBase + what is in this memory location
                    paramPos.append(self.relBase + self.memory[self.progCounter + i, default: 0])
                default:
                    print("Unrecognised parameter type \(paramType)")
                    return nil
                }
            }
            // print(opcode, paramPos)
            switch opcode {
            case 99:
                self.halted = true
                print("Halted")
                if self.output.count > 0 {
                    return self.output
                }
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 + in2
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 * in2
            case 3:
                // Keep passing the input character by character, unless we've
                // finished, and it's a new prompt.  In which case fill up our
                // buffer again, and restart the process
                if self.input.count < 1 {
                    // Need input!
                    self.progCounter -= 1
                    let output = self.output
                    self.output = []
                    return output
                }
                self.memory[paramPos[0]] = self.input.removeFirst()
                self.progCounter += 1
            case 4:
                // Update output
                self.output.append(self.memory[paramPos[0], default: 0])
                self.progCounter += 1
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0], default: 0] < self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 8:
                // equals
                if self.memory[paramPos[0], default: 0] == self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 9:
                // Adjust relative base
                self.relBase += self.memory[paramPos[0], default: 0]
                self.progCounter += 1
            default:
                print("Unrecognised opcode \(opcode)")
                return nil
            }
        }
    }
}

var code: [Int] = []
let inputFile = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input")
let input = try String.init(contentsOfFile: inputFile)
.components(separatedBy: "\n")[0]
for char in input.components(separatedBy: ",") {
    if let int = Int(char) {
        code.append(int)
    }
}

let droid = IntCode(code)

func instructions() {
    print("There are special instructions as this day is a bit manual")
    print("You need to wander around, picking up all the items,")
    print("then make your way to the Security Checkpoint")
    print("Do not pick up any of these trap items:")
    print("infinite loop, molten lava, photons, giant electromagnet, escape pod")
    print("\nIf you have saved a bunch of commands required into a file,")
    print("you can just run them using 'run day25-commands.txt'")
    print()
    print("Once you get to the Security Checkpoint with *all* your")
    print("items, you can type 'solve north' (if the room is to the north)")
    print("This will run 'inv' to get a list of what items you have, and play")
    print("the combinations.")
}

func lineToInt(_ line: String) -> [Int] {
    var ints = [Int]()
    for char in line {
        if let ascii = char.asciiValue {
            ints.append(Int(ascii))
        }
    }
    ints.append(10)
    return ints
}

func intsToStr(_ ints: [Int]) -> String {
    var msg = ""
    for integer in ints {
        if let char = UnicodeScalar(integer) {
            msg += String(Character(char))
        }
    }
    return msg
}

func getInventory() -> [String] {
    // Get list of items
    let inv = lineToInt("inv")
    // Now look for the items
    var items: [String] = []
    var itemList = false
    if let ints = droid.run(inv) {
        let lines = intsToStr(ints)
        for line in lines.components(separatedBy: "\n") {
            if line.starts(with: "Items in your inventory") {
                itemList = true
            }
            if itemList && line.starts(with: "- ") {
                items.append(line.components(separatedBy: "- ")[1])
            }
        }
    }
    return items
}

func solve(_ room: String) {
    let roomInt = lineToInt(room)
    let items = getInventory()
    print("You have \(items.count) items:")
    print(items)
    // Ordinarily for combinations, we would use a binary sequence.  However,
    // there is a cost associated with a change, so rather than going with a
    // traditional binary sequence, we'll go with a "gray code"
    // eg, if we had three objects, we start holding all the items:
    //   Binary  Gray
    //   A B C   A B C
    //   1 1 1   1 1 1  Start holding all items
    //   1 1 0   1 1 0  Drop C
    //   1 0 1   1 0 0  In Binary, we drop B and pick up C; in Gray, just drop B
    // As each transaction with IntCode has a cost, we minimise this cost by
    // only doing one move per iteration - which is what we get with Gray

    print("Running through the combinations, this may take a while...")
    var holding = Array(repeating: true, count: items.count)

    let transitions = GrayChanges(bits: items.count, state: true)

    for (number, take) in transitions {
        var verb = "drop"
        if take {
            verb = "take"
        }
        let noun = items[number]

        let action = lineToInt("\(verb) \(noun)")

        if droid.run(action) == nil {
            print("Something went wrong!")
            break
        }
        holding[number] = take

        if let result = droid.run(roomInt) {
            let message = intsToStr(result)
            if !message.contains("ejected") {
                print("The winning combination was:")
                for itemID in 0..<items.count where holding[itemID] {
                    print("\(items[itemID])", terminator: " ")
                }
                print()
                print(message)
                break
            }
        }
        if droid.halted {
            print("Complete!")
            break
        }
    }
}

func part1() {
    // Load the program, and just run it.
    instructions()
    print("Running...")
    var input: [Int] = []

    var sensitiveRoom: String?
    while true {
        if let output = droid.run(input) {

            print(intsToStr(output))

            if let command = readLine() {
                if command.starts(with: "run ") {
                    let filename = command.components(separatedBy: " ")[1]
                    do {
                        let commands = try String.init(contentsOfFile: filename)
                        for line in commands.components(separatedBy: "\n") where line.count > 1 {
                            input += lineToInt(line)
                        }
                        continue
                    } catch {
                        print("Unable to read file \(filename)")
                        continue
                    }
                }
                if command.starts(with: "solve ") {
                    sensitiveRoom = command.components(separatedBy: " ")[1]
                    break
                }
                input = lineToInt(command)
            }
        } else {
            // Probably halted
            break
        }
    }
    // Solve!
    if let room = sensitiveRoom {
        solve(room)
    }
}

part1()
