#!/usr/bin/env swift

import Foundation

// Test driven programming... set up the tests first :-)

struct TestEntry {
    var code: [Int]
    var input: [Int]
    var answer: Int
}

var regressionTests: [TestEntry] = []
regressionTests.append(TestEntry(code: [3,9,8,9,10,9,4,9,99,-1,8], input: [3], answer: 0))
regressionTests.append(TestEntry(code: [3,9,8,9,10,9,4,9,99,-1,8], input: [8], answer: 1))
regressionTests.append(TestEntry(code: [3,9,7,9,10,9,4,9,99,-1,8], input: [3], answer: 1))
regressionTests.append(TestEntry(code: [3,9,7,9,10,9,4,9,99,-1,8], input: [9], answer: 0))
regressionTests.append(TestEntry(code: [3,3,1108,-1,8,3,4,3,99], input: [9], answer: 0))
regressionTests.append(TestEntry(code: [3,3,1108,-1,8,3,4,3,99], input: [8], answer: 1))
regressionTests.append(TestEntry(code: [3,3,1107,-1,8,3,4,3,99], input: [8], answer: 0))
regressionTests.append(TestEntry(code: [3,3,1107,-1,8,3,4,3,99], input: [7], answer: 1))
regressionTests.append(TestEntry(code: [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input: [7], answer: 1))
regressionTests.append(TestEntry(code: [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input: [0], answer: 0))
regressionTests.append(TestEntry(code: [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input: [0], answer: 0))
regressionTests.append(TestEntry(code: [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input: [7], answer: 1))
regressionTests.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: [7], answer: 999))
regressionTests.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: [8], answer: 1000))
regressionTests.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: [9], answer: 1001))


var testSuite1: [TestEntry] = []
testSuite1.append(TestEntry(code: [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], input: [4,3,2,1,0], answer: 43210))
testSuite1.append(TestEntry(code: [3,23,3,24,1002,24,10,24,1002,23,-1,23,
101,5,23,23,1,24,23,23,4,23,99,0,0], input: [0,1,2,3,4], answer: 54321))
testSuite1.append(TestEntry(code: [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0], input: [1,0,4,3,2], answer: 65210))

var testSuite2: [TestEntry] = []
testSuite2.append(TestEntry(code: [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5], input: [9,8,7,6,5], answer: 139629729))
testSuite2.append(TestEntry(code: [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10], input: [9,7,8,5,6], answer: 18216))
class IntCode {
    var halted: Bool
    var memory: [Int]
    var input: Int
    var output: Int
    var pc: Int
    var initInput: Bool
    init(code: [Int], input: Int) {
        self.halted = false // We halt when we hit 99
        self.memory = code
        self.input = input
        self.initInput = false
        self.pc = 0
        self.output = 0
    }
    func run(_ input: Int) -> Int? {
        if self.halted {
            return nil
        }
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            var codestr = String(self.memory[self.pc])
            let opstr = codestr.suffix(2)
            let op = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.pc += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            // TODO: This is an ugly hacky mess atm...
            // Opcodes 1,2,3,4,5,6,7,8 have at least 1 parameter
            if op>=0 && op<=8 {
                paramPos.append(self.memory[self.pc])
            }
            // Opcodes 1,2 5,6,7,8 have at least 2 parameters
            if (op>=1 && op<=2) || (op>=5 && op<=8) {
                paramPos.append(self.memory[self.pc+1])
            }
            // Opcodes 1,2 7,8 have the full 3 parameters
            if (op>=1 && op<=2) || (op>=7 && op<=8) {
                paramPos.append(self.memory[self.pc+2])
            }
            // The above has assumed positional arguments (0)
            // Now convert to immediate where appropriate
            for i in 0..<codestr.count {
                if codestr.popLast() ?? "0" == "1" {
                    paramPos[i] = self.pc + i
                }
            }
            switch op {
            case 99:
                self.halted = true
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0]]
                let in2 = self.memory[paramPos[1]]
                let outpos = paramPos[2]
                self.pc += 3
                self.memory[outpos] = in1 + in2
                continue
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0]]
                let in2 = self.memory[paramPos[1]]
                let outpos = paramPos[2]
                self.pc += 3
                self.memory[outpos] = in1 * in2
                continue
            case 3:
                // Pass in the current input
                if self.initInput {
                    self.memory[paramPos[0]] = input
                } else {
                    self.memory[paramPos[0]] = self.input
                    self.initInput = true
                }
                self.pc += 1
            case 4:
                // Get output
                self.output = self.memory[paramPos[0]]
                self.pc += 1
                return self.output
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.pc = self.memory[paramPos[1]]
                } else {
                    self.pc += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.pc = self.memory[paramPos[1]]
                } else {
                    self.pc += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0]] < self.memory[paramPos[1]] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.pc += 3
            case 8:
                // equals
                if self.memory[paramPos[0]] == self.memory[paramPos[1]] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.pc += 3
            default:
                print("Unrecognised opcode \(op)")
                return nil
            }
        }
    }
}

var code: [Int] = []
if CommandLine.argc == 2 {
    let input = try String.init(contentsOfFile: CommandLine.arguments[1])
    .components(separatedBy: "\n")[0]
    for n in input.components(separatedBy: ",") {
        if let i = Int(n) {
            code.append(i)
        }
    }
}

func part1(code: [Int], inputs: [Int]) -> Int {
    var amps: [IntCode] = []
    for i in inputs {
        let amp = IntCode(code: code, input: i)
        amps.append(amp)
    }
    var output = 0
    for amp in amps {
        output = amp.run(output)!
    }
    return output
}

func part2(code: [Int], inputs: [Int]) -> Int {
    var amps: [IntCode] = []
    for i in inputs {
        let amp = IntCode(code: code, input: i)
        amps.append(amp)
    }
    var outputE = 0
    var output = 0
    while true {
        for i in 0..<amps.count {
            let amp = amps[i]
            if amp.halted {
                return outputE
            }
            // output is nil when halted
            if let out = amp.run(output) {
                output = out
                if i == (amps.count - 1) {
                    outputE = output
                }
            } else {
                return outputE
            }
        }
    }
    return outputE
}

// Run some tests

var passes = 0

// Check the regression tests first
for t in regressionTests {
    let code = t.code
    let guess = part1(code: code, inputs: t.input)
    if guess != t.answer {
        print("Wrong guess \(guess) in test \(t)")
    } else {
        passes += 1
        print("Passed regression test \(passes)")
    }
}

passes = 0
for t in testSuite1 {
    let code = t.code
    if part1(code: code, inputs: t.input) != t.answer {
        print("Failed testSuite1 \(t)")
    } else {
        passes += 1
        print("Passed testSuite1 \(passes)")
    }
}

if passes == testSuite1.count {
    print("100% pass!")
    var maxThrust = 0
    for i1 in 0...4 {
        for i2 in 0...4 {
            if i2 == i1 {
                continue
            }
            for i3 in 0...4 {
                if i3 == i1 || i3 == i2 {
                    continue
                }
                for i4 in 0...4 {
                    if i4 == i1 || i4 == i2 || i4 == i3 {
                        continue
                    }
                    for i5 in 0...4 {
                        if i5 == i1 || i5 == i2 || i5 == i3 || i5 == i4 {
                            continue
                        }
                        let thrust = part1(code: code, inputs: [i1, i2, i3, i4, i5])
                        if thrust > maxThrust {
                            maxThrust = thrust
                        }
                    }
                }
            }
        }
    }
    print("Part1: \(maxThrust)")
} else {
    print("You have more fixes to do!")
}

passes = 0
for t in testSuite2 {
    let guess = part2(code: t.code, inputs: t.input)
    if guess != t.answer {
        print("Wrong guess \(guess) for \(t)")
    } else {
        passes += 1
        print("Passed testSuite2 \(passes)")
    }
}
if passes == testSuite2.count {
    print("100% pass!")
    var maxThrust = 0
    for i1 in 5...9 {
        for i2 in 5...9 {
            if i2 == i1 {
                continue
            }
            for i3 in 5...9 {
                if i3 == i1 || i3 == i2 {
                    continue
                }
                for i4 in 5...9 {
                    if i4 == i1 || i4 == i2 || i4 == i3 {
                        continue
                    }
                    for i5 in 5...9 {
                        if i5 == i1 || i5 == i2 || i5 == i3 || i5 == i4 {
                            continue
                        }
                        let thrust = part2(code: code, inputs: [i1, i2, i3, i4, i5])
                        if thrust > maxThrust {
                            maxThrust = thrust
                        }
                    }
                }
            }
        }
    }
    print("Part2: \(maxThrust)")
} else {
    print("You have more fixes to do!")
}
