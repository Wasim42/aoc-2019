#!/usr/bin/env swift

func isValid1(password: String) -> Bool {
    // All the rules are textual...
    var hasDouble = false
    var highest: Character = "0"    // Strictly we should be using an optional
                                    // but we know that the input starts with a
                                    // "1"
    var wrongWay = false
    password.forEach {
        if $0 < highest {
            // We've gone back a character!
            wrongWay = true
            return
        }
        if $0 == highest {
            hasDouble = true
        }
        highest = $0
    }
    return hasDouble && !wrongWay
}

func isValid2(password: String) -> Bool {
    // All the rules are textual...
    var hasDouble = false
    var highest: Character = "0"    // Strictly we should be using an optional
                                    // but we know that the input starts with a
                                    // "1"
    var wrongWay = false
    var repeats = 0

    password.forEach {
        if $0 < highest {
            // We've gone back a character!
            wrongWay = true
            return
        }
        if $0 == highest {
            repeats += 1
        }
        if $0 != highest {
            if repeats == 1 {
                hasDouble = true
            }
            repeats = 0
        }
        highest = $0
    }
    // Note that there may be the last two digits that are repeats... but the
    // above loop would have already finished!
    if repeats == 1 {
        hasDouble = true
    }
    return hasDouble && !wrongWay
}

var pw = "111111"
print("\(pw): \(isValid2(password: pw))")
pw = "112233"
print("\(pw): \(isValid2(password: pw))")
pw = "123444"
print("\(pw): \(isValid2(password: pw))")
pw = "111122"
print("\(pw): \(isValid2(password: pw))")

var valids1 = 0
var valids2 = 0
for input in 347312...805915 {
    let password = String(input)
    if isValid1(password: password) {
        valids1 += 1
    }
    if isValid2(password: password) {
        valids2 += 1
    }
}

print("Valid passwords part1: \(valids1)")
print("Valid passwords part2: \(valids2)")
