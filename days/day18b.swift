#!/usr/bin/env swift

import Foundation

let inputFile = "day18.input"
let input1 = try String.init(contentsOfFile: inputFile)

let inputFile2 = "day18.input2"
let input2 = try String.init(contentsOfFile: inputFile)

var part1Cases: [(String, Int)] = []
part1Cases.append(("""
                   #########
                   #b.A.@.a#
                   #########
                   """, 8))
part1Cases.append(("""
                   ########################
                   #f.D.E.e.C.b.A.@.a.B.c.#
                   ######################.#
                   #d.....................#
                   ########################
                   """, 86))
part1Cases.append(("""
                   ########################
                   #...............b.C.D.f#
                   #.######################
                   #.....@.a.B.c.d.A.e.F.g#
                   ########################
                   """, 132))
part1Cases.append(("""
                   #################
                   #i.G..c...e..H.p#
                   ########.########
                   #j.A..b...f..D.o#
                   ########@########
                   #k.E..a...g..B.n#
                   ########.########
                   #l.F..d...h..C.m#
                   #################

                   """, 136))
part1Cases.append(("""
                   ########################
                   #@..............ac.GI.b#
                   ###d#e#f################
                   ###A#B#C################
                   ###g#h#i################
                   ########################
                   """, 81))

class Graph {

    var matrix: [Character: [Character: Int]]
    var vertices: Set<Character>
    var directed: Bool

    init(_ vertices: Set<Character>, directed: Bool = false) {
        self.directed = directed
        self.vertices = vertices
        self.matrix = [:]
        // Initialise the adjacency matrix
        for vertex in vertices {
            var array: [Character: Int] = [:]
            for column in vertices {
                array[column] = 0
            }
            matrix[vertex] = array
        }
    }
    // swiftlint:disable identifier_name
    func addEdge(from: Character, to: Character, weight: Int = 1) {
        // swiftlint:enable identifier_name
        guard weight != 0 else {
            print("Invalid weight \(weight)")
            return
        }
        guard let row = matrix[from] else {
            print("Unknown vertex \(from)")
            return
        }
        guard row[to] != nil else {
            print("Unknown vertex \(to)")
            return
        }
        matrix[from]![to]! = weight
        if !directed {
            matrix[from]![to]! = weight
        }
    }
    // swiftlint:disable identifier_name
    func getWeight(from: Character, to: Character) -> Int? {
        // swiftlint:enable identifier_name
        guard let row = matrix[from] else {
            print("Unknown vertex \(from)")
            return nil
        }
        return row[to]
    }

    func getAdjacentVertices(to vertex: Character) -> [Character] {
        var adjacencies: [Character] = []

        guard let row = matrix[vertex] else {
            print("Unknown vertex \(vertex)")
            return adjacencies
        }
        for key in self.vertices {
            guard let weight = row[key] else {
                print("Internal error! Can't find (\(vertex), \(key))")
                return adjacencies
            }
            if weight != 0 {
                adjacencies.append(key)
            }
        }
        return adjacencies
    }
}

class PQNode: Hashable, Equatable {
    var moves: Int
    var keys: Set<Character>
    var location: Character

    init(pos: Character, moves: Int, keys: Set<Character>) {
        self.moves = moves
        self.keys = keys
        location = pos
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(moves)
        hasher.combine(keys)
        hasher.combine(location)
    }
    static func == (lhs: PQNode, rhs: PQNode) -> Bool {
        return lhs.moves == rhs.moves
    }
}

class PriorityQueue {
    private var _queue: [Int: [PQNode]]
    private var _keys: [Int]

    var count: Int
    var isEmpty: Bool {
        return count == 0
    }
    init() {
        _queue = [:]
        _keys = []
        count = 0
    }

    // We push onto the end of the priority queue
    func push(pos: Character, moves: Int, keys: Set<Character>) {
        let newNode = PQNode(pos: pos, moves: moves, keys: keys)
        count += 1
        _queue[newNode.moves, default: []].append(newNode)
        if !_keys.contains(newNode.moves) {
            _keys.append(newNode.moves)
            _keys.sort()
        }
    }
    // We pop from the front of the lowest numbered list
    func pop() -> PQNode? {
        let lowest = _keys[0]
        count -= 1
        let node = _queue[lowest]!.removeFirst()
        if _queue[lowest]!.count < 1 {
            _queue[lowest] = nil
            _ = _keys.removeFirst()
        }
        return node
    }
}

func getMazeItems(_ input: String) -> ([[Character]], [Character: CoOrds]) {

    // Step 1 - convert the multi-line string into an array of characters so we
    // have a chance of navigating it.  While we're doing this, get a list and
    // location of significant items

    var maze: [[Character]] = []
    var items: [Character: CoOrds] = [:]

    var line: [Character] = []
    var row = 0
    var col = 0

    for char in input {
        if char != "\n" {
            if char != "#" && char != "." {
                // An item of significance
                items[char] = CoOrds(x: col, y: row)
            }
            col += 1
            line.append(char)
        } else {
            row += 1
            col = 0
            maze.append(line)
            line = []
        }
    }
    // We usually don't have a trailing \n
    if line.count > 1 {
        maze.append(line)
    }

    return (maze, items)
}

// The only reason why I use CoOrds instead of a tuple is because a tuple isn't
// Hashable.  Come on Swift!  Get your act together!
struct CoOrds: Hashable, CustomStringConvertible {
    // swiftlint:disable identifier_name
    var x: Int
    var y: Int
    // swiftlint:enable identifier_name
    var description: String { return "(\(x), \(y))" }
}

// Consider the maze:
// #########
// #b.A.@.a#
// #########
// This has the edges: b<->A, A<->@, and @<->a
struct Edge: Hashable {
    var one: Character
    var two: Character
    var length: Int
}

class Node: CustomStringConvertible {
    var location: CoOrds
    var start: Character
    var moves: Int
    var next: Node?
    weak var prev: Node?
    var description: String {
        var msg = "Location: \(location), start: \(start), moves: \(moves)\n"
        msg += prev == nil ? "nil" : "prev"
        msg += " <-- --> "
        msg += next == nil ? "nil" : "next"
        return msg
    }
    init(char: Character, loc: CoOrds, num: Int) {
        start = char
        location = loc
        moves = num
    }
}

class Queue {
    var head: Node?
    var tail: Node?
    var count: Int
    init() {
        count = 0
    }
    // In a queue, we push to the end
    func push(char: Character, loc: CoOrds, move: Int) {
        count += 1
        let newNode = Node(char: char, loc: loc, num: move)
        guard head != nil else {
            // This is an empty queue!
            head = newNode
            tail = newNode
            return
        }
        newNode.prev = tail
        tail!.next = newNode
        tail = newNode
    }
    // In a queue, we pop from the front of the queue
    func pop() -> Node? {
        guard let node = head else {
            // No head, there is nothing to pop!
            return nil
        }
        count -= 1

        if let second = node.next {
            // There are more entries, so keep the chain going
            second.prev = nil
            head = second
        } else {
            // There is no further entry in the queue, time to empty it properly
            head = nil
            tail = nil
        }
        return node
    }
}

func getEdges(_ input: String) -> (Set<Edge>, Set<Character>) {

    let (maze, items) = getMazeItems(input)
    let maxX = maze[0].count
    let maxY = maze.count

    // We're going to return the keys
    var vertices: Set<Character> = []

    // Step 2 - get all the route points.
    // We do this by starting at each item, and bfs'ing the neighbours.
    // If either side is a capital, we depend on the lowercase version.

    var edges: Set<Edge> = []

    let bfsQueue = Queue()

    // We have to keep the visited by starting item.  If it's global we'll
    // never get any as both edges will be killed off in the middle
    var visited: [Character: Set<CoOrds>] = [:]

    // Initialise our queue with all the items in the maze
    for (item, loc) in items {
        if item != "#" && item != "." {
            vertices.insert(item)
        }
        bfsQueue.push(char: item, loc: loc, move: 0)
        visited[item] = Set<CoOrds>([loc])
    }

    // Let's bfs!
    let motionAllCases = [(0, 1), (1, 0), (-1, 0), (0, -1)]

    while bfsQueue.count > 0 {
        guard let node = bfsQueue.pop() else {
            break
        }
        for (shiftX, shiftY) in motionAllCases {
            var newPos = node.location
            newPos.x += shiftX
            // Check that we're still in the grid
            guard newPos.x >= 0 && newPos.x < maxX else {
                continue
            }
            newPos.y += shiftY
            guard newPos.y >= 0 && newPos.y < maxY else {
                continue
            }

            let found = maze[newPos.y][newPos.x]
            guard found != "#" else {
                // Hit a wall, not a valid move
                continue
            }

            guard !visited[node.start]!.contains(newPos) else {
                // Already visited
                continue
            }
            // Something interesting!
            if found != "." {
                // We need to add this route
                let newEdge = Edge(one: node.start,
                                   two: found,
                                   length: node.moves+1)
                edges.insert(newEdge)
                continue
            }
            // This is something to consider
            visited[node.start]!.insert(newPos)
            bfsQueue.push(char: node.start,
                          loc: newPos,
                          move: node.moves+1)
        }

    }
    return (edges, vertices)
}

func solve1(_ input: String) -> Int {

    // First thing we need is a graph of our situation
    let (edges, vertices) = getEdges(input)
    let graph = Graph(vertices)
    for edge in edges {
        graph.addEdge(from: edge.one, to: edge.two, weight: edge.length)
    }

    // Get the list of all the keys we're searching for
    var allKeys: Set<Character> = []
    for point in vertices where point.isLowercase {
        allKeys.insert(point)
    }

    // Now we need to bfs this.
    // Note that unlike a normal bfs, we have to keep track of keys!
    // Each time we pick up a new key, the situation has changed enough that
    // our previously visited sites no longer matter.  We need to reset our
    // visited locations on the acquisition of every new key.
    // We do this by keeping a dictionary of our locations (nodes) by the keys
    // we have in our posession.
    // AFAICT the ordering of the keys *shouldn't* matter.
    var visited: [Set<Character>: Set<Character>] = [:]

    let queue = PriorityQueue()
    // Put in our starting position...
    queue.push(pos: "@", moves: 0, keys: [])
    visited[[]] = ["@"]

    while queue.count > 0 {
        let state = queue.pop()!
        // Check what adjacent nodes there are
        for nextNode in graph.getAdjacentVertices(to: state.location) {
            // Have we visited this node already?
            guard !visited[state.keys]!.contains(nextNode) else {
                // Already visited - no good for us
                continue
            }
            var keychain = state.keys
            // Is this a locked door to us?
            if nextNode.isUppercase && !keychain.contains(Character(nextNode.lowercased())) {
                // You cannot come in, not by the hairs of my chinny chin chin!
                continue
            }
            // If this is a key, just add it to our keychain
            // We use a set, so duplicates disappear!
            if nextNode.isLowercase {
                keychain.insert(nextNode)
            }
            // Have we already been here from another direction?
            guard !visited[keychain, default: []].contains(nextNode) else {
                // We've already visited this location with this key from
                // another location - ignore!
                continue
            }
            guard let weight = graph.getWeight(from: state.location, to: nextNode) else {
                // This should be impossible!
                print("Unable to find a weight between \(state.location) and \(nextNode)")
                return -1
            }
            if keychain == allKeys {
                // We've done it!
                return state.moves + weight
            }
            // This is now safe to visit
            visited[keychain, default: []].insert(nextNode)
            queue.push(pos: nextNode, moves: state.moves + weight, keys: keychain)
        }

    }

    print("Oops - didn't find an answer :-(")
    return 0
}

var passes = 0
for (question, answer) in part1Cases {
    let guess = solve1(question)
    if guess == answer {
        print("Expected \(answer) and got it!")
        passes += 1
    } else {
        print("Expected \(answer), but got \(guess) instead")
    }
}

if passes == part1Cases.count {
    print("Part 1: \(solve1(input1))")
}
