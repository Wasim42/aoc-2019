#!/usr/bin/env swift

import Foundation

let inputFile = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input")
let input1 = try String.init(contentsOfFile: inputFile)

let inputFile2 = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input2")
let input2 = try String.init(contentsOfFile: inputFile)

struct TestCase {
    var input: String
    var answer: Int
}

var part1Cases: [TestCase] = []

part1Cases.append(
    TestCase(input: """
             #########
             #b.A.@.a#
             #########
             """,
             answer: 8
             )
    )

part1Cases.append(
    TestCase(input: """
             ########################
             #f.D.E.e.C.b.A.@.a.B.c.#
             ######################.#
             #d.....................#
             ########################
             """,
             answer: 86
             )
    )

part1Cases.append(
    TestCase(input: """
             ########################
             #...............b.C.D.f#
             #.######################
             #.....@.a.B.c.d.A.e.F.g#
             ########################
             """,
             answer: 132
             )
    )

part1Cases.append(
    TestCase(input: """
             #################
             #i.G..c...e..H.p#
             ########.########
             #j.A..b...f..D.o#
             ########@########
             #k.E..a...g..B.n#
             ########.########
             #l.F..d...h..C.m#
             #################
             """,
             answer: 136
             )
    )

part1Cases.append(
    TestCase(input: """
             ########################
             #@..............ac.GI.b#
             ###d#e#f################
             ###A#B#C################
             ###g#h#i################
             ########################
             """,
             answer: 81
             )
    )

var part2Cases: [TestCase] = []

part2Cases.append(
    TestCase(input: """
             #######
             #a.#Cd#
             ##@#@##
             #######
             ##@#@##
             #cB#Ab#
             #######
             """,
             answer: 8
             )
    )

part2Cases.append(
    TestCase(input: """
             ###############
             #d.ABC.#.....a#
             ######@#@######
             ###############
             ######@#@######
             #b.....#.....c#
             ###############
             """,
             answer: 24
             )
    )

part2Cases.append(
    TestCase(input: """
             #############
             #DcBa.#.GhKl#
             #.###@#@#I###
             #e#d#####j#k#
             ###C#@#@###J#
             #fEbA.#.FgHi#
             #############
             """,
             answer: 32
             )
    )

part2Cases.append(
    TestCase(input: """
             #############
             #g#f.D#..h#l#
             #F###e#E###.#
             #dCba@#@BcIJ#
             #############
             #nK.L@#@G...#
             #M###N#H###.#
             #o#m..#i#jk.#
             #############
             """,
             answer: 72
             )
    )

// Thank you reddit - in particular
// https://www.reddit.com/r/swift/comments/936cj4/practicing_functional_swift_by_creating_a/e3bz9ce/
extension MutableCollection where Self: BidirectionalCollection {
    private mutating func permute(indices: Indices, flag: Bool, result: inout [Self]) {
        guard let lastIndex = indices.last, lastIndex != startIndex else {
            result.append(self)
            return
        }

        for index in indices {
            permute(indices: indices.dropLast(), flag: !flag, result: &result)
            swapAt(flag ? index : startIndex, lastIndex)
        }
    }

    func permute() -> [Self] {
        var copy = self
        var result: [Self] = []
        copy.permute(indices: indices, flag: false, result: &result)
        return result
    }
}

struct CoOrds: Hashable, Equatable {
    // swiftlint:disable identifier_name
    var x: Int
    var y: Int
    // swiftlint:enable identifier_name
}

func makeMaze(_ input: String) -> [[Character]] {
    // We make our maze an array of character arrays.  It's much easier to
    // handle than a list of strings as we don't need to worry about indexing!
    var maze: [[Character]] = []
    var line: [Character] = []
    for char in input {
        if char == "\n" {
            maze.append(line)
            line = []
            continue
        }
        line.append(char)
    }
    maze.append(line)

    return maze
}

struct State: Hashable, Equatable {
    var positions: [CoOrds]
    var moves: Int
    var foundKeys: Set<Character>
    var foundKeysOrder: [Character]
}

class Node {
    var data: State
    var next: Node?
    weak var previous: Node?
    init(_ value: State) {
        data = value
    }
}

class Queue: CustomStringConvertible {
    var head: Node?
    var tail: Node?
    var count: Int

    public var moves: Int {
        guard let first = head else {
            return 0
        }
        return first.data.moves
    }
    public var isEmpty: Bool {
        return head == nil
    }
    public var first: Node? {
        return head
    }
    public var last: Node? {
        return tail
    }
    private var lock: NSLock
    var description: String {
        if head == nil && tail == nil {
            return "(nil) -> (nil)"
        }
        if head == nil {
            return "(nil) ->"
        }
        if tail == nil {
            return "-> (nil)"
        }
        var node = head
        var msg = ""
        while node != nil {
            msg += "(\(node!.data.moves)) ->"
            node = node!.next
        }
        return msg + " (nil)"
    }
    init() {
        head = nil
        tail = nil
        count = 0
        lock = NSLock()
    }
    func syncAppend(_ data: State) {
        lock.lock()
        defer { lock.unlock() }
        self.append(data)
    }
    // One of the reasons why we append to the end of the queue is because the
    // number of moves is always incrementing - so the earlier entries in the
    // queue have fewer moves.  However it's possible that the latest being
    // appended has fewer moves than the previous one.  So we need to keep
    // stepping back until we're at the right number of moves.
    func append(_ data: State) {
        let newNode = Node(data)
        count += 1

        guard tail != nil else {
            head = newNode
            tail = newNode
            return
        }

        // We need to insert to the end of the list where moves matches
        var prev, next: Node?
        prev = tail

        while true {
            if prev == nil {
                break
            }
            if prev!.data.moves <= newNode.data.moves {
                break
            }
            next = prev
            prev = next!.previous
        }

        if prev == nil {
            head = newNode
        } else {
            newNode.next = prev!.next
            prev!.next = newNode
        }
        newNode.previous = prev

        if next == nil {
            tail = newNode
        } else {
            next!.previous = newNode
        }
        newNode.next = next
    }
    func syncPop() -> State? {
        lock.lock()
        defer { lock.unlock() }
        return self.pop()
    }
    func pop() -> State? {
        guard let node = head else {
            print("Nothing to pop!")
            print("Count: \(count)")
            print("self.tail nullness: \(self.tail == nil)")
            return nil
        }
        count -= 1
        if count == 0 {
            self.head = nil
            self.tail = nil
            return node.data
        }
        self.head = node.next
        if self.head != nil {
            self.head!.previous = nil
        } else {
            print("We lost the head with \(count) members still around!")
        }
        return node.data
    }
}

func printMaze(_ maze: [[Character]], _ inState: State? = nil) {

    var poses: [CoOrds]
    if let state = inState {
        poses = state.positions

    } else {
        poses = []
        poses.append(CoOrds(x: -1, y: -1))
    }
    for posY in 0..<maze.count {
        let line = maze[posY]
        for posX in 0..<line.count {
            var char = line[posX]
            for pos in poses {
                if posX == pos.x && posY == pos.y {
                    char = "X"
                }
            }
            print(char, terminator: "")
        }
        print()
    }
}

// To solve this we need to use a breadth-first search...
// Ordinarily bfs only keeps track of whether we've already visited a position,
// and doesn't look somwhere we've already visited.  However we're going to
// have different views of an (x, y) location - depending on whether we had a
// key, or not.  So, for example, 'A' is either a dead-end or not depending on
// whether we have the key 'a'.  This is going to be a little mind-bending :-(

// Note: All we care about is getting all the keys - so there may be doors for
// which we have no keys - we don't need to worry about them.
// swiftlint:disable function_body_length
// swiftlint:disable cyclomatic_complexity
func solve1(maze: [[Character]]) -> Int {
    // Maze attributes
    let mazeX = maze[0].count
    let mazeY = maze.count
    // What keys are we searching for?
    var keys: Set<Character> = []
    // And from where?
    var foundPos: CoOrds?
    for lineNo in 0..<mazeY {
        if maze[lineNo].count < 1 {
            // Faulty data - seems to happen :-(
            continue
        }
        for charNo in 0..<mazeX {
            let char = maze[lineNo][charNo]
            if char.isLowercase {
                // This is a valid key
                keys.insert(char)
            } else if char == "@" {
                foundPos = CoOrds(x: charNo, y: lineNo)
            }
        }
    }
    guard let startPos = foundPos else {
        print("Didn't find the starting position!")
        return 0
    }
    print("Searching for \(keys)")

    // Now let's do a breadth-first search until we retrieve all the keys
    let queue = Queue()

    queue.append(
        State(
            positions: [startPos], moves: 0, foundKeys: [], foundKeysOrder: []
            )
        )

    struct Visit: Hashable {
        var keys: Set<Character>
        var pos: CoOrds
    }
    var visited: Set<Visit> = []

    // This is the equivalent of while true, as we expect to hit find all our
    // keys before the queue depletes!
    var runs = 0
    let motionAllCases = [(0, 1), (1, 0), (-1, 0), (0, -1)]

    while queue.count > 0 {
        runs += 1
        if runs.isMultiple(of: 1000000) {
            print(runs, queue.moves, queue.count, visited.count)
        }
        guard let state = queue.pop() else {
            print("We've stopped after \(runs) runs!")
            break
        }
        let pos = state.positions[0]

        let here = maze[pos.y][pos.x]
        var foundKeys = state.foundKeys
        var moves = state.moves

        if here != "." && keys.contains(here) && !foundKeys.contains(here) {
            // A new key has been found!
            foundKeys.insert(here)
            // print(moves)
            // printMaze(maze, state)
            if foundKeys.count == keys.count {
                // Tada!  We've finished!
                return moves
            }
        }
        moves += 1

        // Now examine all possible moves from here.
        for (shiftX, shiftY) in motionAllCases {

            var newPos = pos
            newPos.x += shiftX
            newPos.y += shiftY

            if newPos.x < 0 || newPos.x > mazeX {
                // Invalid x position!
                continue
            }
            if newPos.y < 0 || newPos.y > mazeY {
                // Invalid y position!
                continue
            }

            if maze[newPos.y][newPos.x] == "#" {
                // Wall!
                continue
            }
            // Check for a locked door!
            if here.isUppercase {
                let requiredKey = Character(here.lowercased())
                if !foundKeys.contains(requiredKey) {
                    // No key, no move!
                    continue
                }
            }
            let newVisit = Visit(keys: foundKeys, pos: newPos)
            if visited.contains(newVisit) {
                continue
            }
            visited.insert(newVisit)

            // This is a valid move - add it to the queue
            let newState = State(
                positions: [newPos],
                moves: moves,
                foundKeys: foundKeys,
                foundKeysOrder: []
            )
            queue.append(newState)
        }

    }
    return 0
}

func solve2(maze: [[Character]]) -> Int {

    // For our breadth-first search, we need to set up the queue
    let queue = Queue()

    var startState = State(positions: [], moves: 0, foundKeys: [], foundKeysOrder: [])

    // Maze attributes
    let mazeX = maze[0].count
    let mazeY = maze.count
    // What keys are we searching for?
    var keys: Set<Character> = []
    // And from where?
    for lineNo in 0..<mazeY {
        if maze[lineNo].count < 1 {
            // Faulty data - seems to happen :-(
            continue
        }
        for charNo in 0..<mazeX {
            let char = maze[lineNo][charNo]
            if char.isLowercase {
                // This is a valid key
                keys.insert(char)
            } else if char == "@" {
                startState.positions.append(CoOrds(x: charNo, y: lineNo))
            }
        }
    }

    queue.append(startState)

    let posPerms = [Int](0..<startState.positions.count).permute().flatMap { $0 }

    print("Searching for \(keys)")

    struct Visit: Hashable {
        var keys: [Character]
        var pos: CoOrds
    }
    var visited: Set<Visit> = []

    var runs = 0
    let motionAllCases = [(0, 0), (0, 1), (1, 0), (-1, 0), (0, -1)]

    // This is the equivalent of while true, as we expect to hit find all our
    // keys before the queue depletes!
    while queue.count > 0 {
        // We need to keep track of all possible robot moves...
        // This includes all combinations of which robot to move!
        guard let state = queue.pop() else {
            print("We've stopped after \(runs) runs!")
            break
        }
        // Here's a tricky thing - we can't tell in which order the robots will
        // need to move.  Sometimes we'll need robot 3 to move three times
        // before robot 2 moves once for us to achieve our goal.
        // This is why we have the "non-move" in the motionAllCases above.  It
        // allows us to get to a situation where robot 3 has moved three while
        // robot 2 hasn't moved at all!
        // Now examine all possible moves from here.
        for (shiftX, shiftY) in motionAllCases {
            for posNo in posPerms {

                runs += 1
                if runs.isMultiple(of: 1000000) {
                    print(runs, queue.moves, queue.count, visited.count)
                }

                let pos = state.positions[posNo]

                let here = maze[pos.y][pos.x]
                var foundKeysOrder = state.foundKeysOrder

                if here != "." && keys.contains(here) && !foundKeysOrder.contains(here) {
                    // A new key has been found!
                    foundKeysOrder.append(here)
                    /*
                    if foundKeysOrder.count > 2 && foundKeysOrder[0] == "e" && foundKeysOrder[1] == "h" {
                        print("Moves: \(state.moves), queue.count: \(queue.count)")
                        printMaze(maze, state)
                    }
                    */
                    if foundKeysOrder.count == keys.count {
                        // Tada!  We've finished!
                        print("We completed after \(runs) runs: ", foundKeysOrder)
                        return state.moves
                    }
                }

                var moves = state.moves
                if shiftX != 0 || shiftY != 0 {
                    // This is not a non-move!
                    moves += 1
                }

                var newPos = pos
                newPos.x += shiftX
                newPos.y += shiftY

                if newPos.x < 0 || newPos.x > mazeX {
                    // Invalid x position!
                    continue
                }
                if newPos.y < 0 || newPos.y > mazeY {
                    // Invalid y position!
                    continue
                }

                if maze[newPos.y][newPos.x] == "#" {
                    // Wall!
                    continue
                }
                // Check for a locked door!
                if here.isUppercase {
                    let requiredKey = Character(here.lowercased())
                    if !foundKeysOrder.contains(requiredKey) {
                        // No key, no move!
                        continue
                    }
                }
                let newVisit = Visit(keys: foundKeysOrder, pos: newPos)
                if visited.contains(newVisit) {
                    continue
                }
                visited.insert(newVisit)
                // This is a valid move - add it to the queue
                var newPositions = state.positions
                newPositions[posNo] = newPos
                let newState = State(
                    positions: newPositions,
                    moves: moves,
                    foundKeys: [],
                    foundKeysOrder: foundKeysOrder
                )
                queue.append(newState)
            }

        }
    }
    return 0
}

func part1(_ input: String) -> Int? {

    let maze = makeMaze(input)

    return solve1(maze: maze)
}

func part2(_ input: String) -> Int? {

    let maze = makeMaze(input)

    return solve2(maze: maze)
}

var passes = 0
print("Testing part 1...")
for test in part1Cases {
    if let guess = part1(test.input) {
        if guess == test.answer {
            print("Pass!")
            passes += 1
        } else {
            print("\(guess) != \(test.answer)")
        }
    }
}

print("Testing part 2...")
for test in part2Cases {
    if let guess = part2(test.input) {
        if guess == test.answer {
            print("Pass!")
            passes += 1
        } else {
            print("\(guess) != \(test.answer)")
        }
    }
}

if passes == part1Cases.count + part2Cases.count {
    print("100% Pass - well done!")
    if let guess = part1(input1) {
        print("Part 1:", guess)
    }
    /*
    if let guess = part2(input2) {
        print("Part 2:", guess)
    }
    */
} else {
    print("Sorry - you have failures!")
}
