#!/usr/bin/env swift

import Foundation

func part1(noun: Int?, verb: Int?, input: inout [Int]) -> Int? {
    // Initialise the noun and verb
    if let n = noun {
        input[1] = n
    }
    if let v = verb {
        input[2] = v
    }
    var pos = 0
    while true {
        let op = input[pos]
        if op == 99 {
            break
        }
        let in1 = input[input[pos+1]]
        let in2 = input[input[pos+2]]
        let outpos = input[pos+3]
        pos += 4

        // Process the opcode
        switch op {
        case 1:
            input[outpos] = in1 + in2
        case 2:
            input[outpos] = in1 * in2
        default:
            print("Unrecognised opcode \(op)")
            return nil
        }
    }
    return input[0]
}

func part2(_ input: [Int]) -> Int? {
    for noun in 0...99 {
        for verb in 0...99 {
            var newInput = input
            if part1(noun: noun, verb: verb, input: &newInput) == 19690720 {
                return noun * 100 + verb
            }
        }
    }
    print("Unable to find the values...")
    return nil
}

func day2() -> [Int]? {
    do {
        var numbers: [String]

        if CommandLine.argc > 2 ||
        CommandLine.argc > 1 && CommandLine.arguments[1].hasPrefix("-") {
            // The equivalent of --help
            print("\(CommandLine.arguments[0]) [filename]")
            print("\n   If a filename is not provided, we read from stdin")
            return nil
        } else if CommandLine.argc == 2 {
            // We have been given the filename to use
            let nums: [String] = try String.init(contentsOfFile: CommandLine.arguments[1])
            .components(separatedBy: "\n")
            numbers = nums
        } else {
            // Read from stdin continually until no more input.
            print("Please enter your input - blank line to finish:")
            numbers = []
            while true {
                if let line = readLine() {
                    if line.count < 1 {
                        // Blank line - finish
                        break
                    }
                    numbers.append(line)
                } else {
                    // ctrl-d - finish
                    break
                }
            }
        }

        // This is a long string... so we need to convert it to an array of int's
        var input: [Int] = []
        let strings = numbers[0].components(separatedBy: ",")
        for s in strings {
            if let i = Int(s) {
                input.append(i)
            }
        }

        print(input)
        var noun: Int?
        var verb: Int?
        if CommandLine.argc > 1 {
            // Using the input file, so we need to set the noun and verb
            noun = 12
            verb = 2
        }

        var newInput = input
        if let answer = part1(noun: noun, verb: verb, input: &newInput) {
            if let answer2 = part2(input) {
                return [answer, answer2]
            }
        } else {
            print("Sorry, I didn't understand")
            return nil
        }

    } catch {
        print("Sorry - I failed to read \(CommandLine.arguments[1])!")
    }
    return nil
}

if let answer = day2() {

    print("Part1: \(answer[0])")
    print("Part2: \(answer[1])")
}
