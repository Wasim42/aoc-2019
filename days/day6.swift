#!/usr/bin/env swift

import Foundation

let testData1 = """
COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
"""
let testAnswer1 = 42

let testData2 = """
COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN
"""
let testAnswer2 = 4

class orbital : CustomStringConvertible {
    var name: String
    var parent: orbital?
    var children: [orbital]
    var description: String { return "\(name)(\(children.count))" }
    init(name: String, parent: orbital?) {
        self.name = name
        self.parent = parent
        self.children = []
    }
}

func makeOrbitals(_ input: String) -> [String:orbital] {
    var orbitals: [String:orbital] = [:]

    for line in input.components(separatedBy: "\n") {
        if line.count < 3 {
            // Bad line
            continue
        }
        let strings = line.components(separatedBy: ")")

        let parent = orbitals[strings[0],
                default: orbital(name: strings[0], parent: nil)]
        let child = strings[1]

        let body = orbitals[child, default: orbital(name: child, parent: parent)]
        body.parent = parent
        parent.children.append(body)
        orbitals[parent.name] = parent
        orbitals[body.name] = body
    }
    return orbitals
}

func part1(_ input: String) -> Int {

    let orbitals = makeOrbitals(input)
    // We need to go through each entry in orbitals, and count how many orbits
    // there are until we get to the grandparent

    var totalOrbits = 0

    for b in orbitals {
        var p = b.value
        while p.parent != nil {
            totalOrbits += 1
            p = p.parent! 
        }
    }

    return totalOrbits
}

class OrbitalQueue {
    private var data: [(body: orbital, depth: Int)]
    var count: Int { return self.data.count }
    init() {
        self.data = []
    }
    init(_ value: orbital) {
        self.data = []
        self.data.append((body: value, depth: 0))
    }
    func push(_ value: orbital, depth: Int) {
        self.data.append((body: value, depth: depth))
    }
    func pop() -> (body: orbital, depth: Int) {
        return self.data.removeFirst()
    }
}

func part2(_ input: String) -> Int {

    let orbitals = makeOrbitals(input)

    // Breadth-first search
    var visited: [String: Bool] = [:]
    let you = orbitals["YOU", default: orbital(name: "Unknown", parent: nil)]
    let queue = OrbitalQueue(you)

    visited[you.name] = true
    while queue.count > 0 {
        let value = queue.pop()
        let body = value.body
        let newdepth = value.depth + 1
        if let parent = body.parent {
            // Check for a stop-point
            if parent.name == "SAN" {
                return value.depth - 1
            }
            if !visited[parent.name, default: false] {
                visited[parent.name] = true
                queue.push(parent, depth: newdepth)
            }
        }
        for i in body.children {
            if i.name == "SAN" {
                return value.depth - 1
            }
            if !visited[i.name, default: false] {
                visited[i.name] = true
                queue.push(i, depth: newdepth)
            }
        }
    }
    return 0
}

let answer1 = part1(testData1)
if answer1 == testAnswer1 {
    print("part1 test passed!")
    if CommandLine.argc == 2 {
        let input = try String.init(contentsOfFile: CommandLine.arguments[1])
        print("Part 1 \(part1(input))")
    }
} else {
    print("Failed test - got \(answer1)")
}

let answer2 = part2(testData2)
if answer2 == testAnswer2 {
    print("part2 test passed!")
    if CommandLine.argc == 2 {
        let input = try String.init(contentsOfFile: CommandLine.arguments[1])
        print("Part 2 \(part2(input))")
    }
} else {
    print("Failed test - got \(answer2)")
}
