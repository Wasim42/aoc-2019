#!/usr/bin/env swift

import Foundation

class Cards: CustomStringConvertible {
    var deck: [Int]
    var description: String {
        var msg = ""
        for card in deck {
            msg += String(card) + " "
        }
        return msg
    }
    init(_ number: Int) {
        deck = Array(0..<number)
    }
    func newStack() {
        // Basically reverse the cards
        deck = [Int](deck.reversed())
    }
    func cut(_ number: Int) {
        // The joy of slices
        if number > 0 {
            let cutted = deck[..<number]
            let rest = deck[number...]
            deck = [Int](rest) + [Int](cutted)
        } else {
            let cutted = deck[(deck.count+number)...]
            let rest = deck[..<(deck.count+number)]
            deck = [Int](cutted) + [Int](rest)
        }
    }
    func increment(_ number: Int) {
        var newDeck = deck
        var pointer = 0
        for card in deck {
            newDeck[pointer%deck.count] = card
            pointer += number
        }
        deck = newDeck
    }

    func execute(_ instructions: String) {
        for line in instructions.components(separatedBy: "\n") where line.count > 2 {
            // print(deck)
            // print(line)
            if line.starts(with: "cut") {
                // This is a cut
                let number = Int(line.components(separatedBy: " ")[1])!
                self.cut(number)
                continue
            }
            if line == "deal into new stack" {
                self.newStack()
                continue
            }
            if line.starts(with: "deal with increment ") {
                let number = Int(line.components(separatedBy: "increment ")[1])!
                self.increment(number)
                continue
            }
            print("Unrecognised instruction \(line)")
        }
    }
}

func testCards() {
    let example = Cards(10)
    example.newStack()
    precondition(example.deck == [9, 8, 7, 6, 5, 4, 3, 2, 1, 0], "Failed newStack")
    let ex2 = Cards(10)
    ex2.cut(3)
    precondition(ex2.deck == [3, 4, 5, 6, 7, 8, 9, 0, 1, 2], "Failed cut 3")

    let ex3 = Cards(10)
    ex3.cut(-4)
    precondition(ex3.deck == [6, 7, 8, 9, 0, 1, 2, 3, 4, 5], "Failed cut -4")

    let ex4 = Cards(10)
    ex4.increment(3)
    precondition(ex4.deck == [0, 7, 4, 1, 8, 5, 2, 9, 6, 3], "Failed increment 3")

    let ex5 = Cards(10)
    ex5.increment(9)
    precondition(ex5.deck == [0, 9, 8, 7, 6, 5, 4, 3, 2, 1], "Failed increment 9, got \(ex5.deck)")
    print("Passed basic tests")
}

testCards()

struct TestEntry {
    var instructions: String
    var answer: [Int]
}
var tests: [TestEntry] = []

tests.append(TestEntry(instructions: """
                       deal with increment 7
                       deal into new stack
                       deal into new stack
                       """,
                       answer: [0, 3, 6, 9, 2, 5, 8, 1, 4, 7]
                       ))
tests.append(TestEntry(instructions: """
                       cut 6
                       deal with increment 7
                       deal into new stack
                       """,
                       answer: [3, 0, 7, 4, 1, 8, 5, 2, 9, 6]
                       ))
tests.append(TestEntry(instructions: """
                       deal with increment 7
                       deal with increment 9
                       cut -2
                       """,
                       answer: [6, 3, 0, 7, 4, 1, 8, 5, 2, 9]
                       ))
tests.append(TestEntry(instructions: """
                       deal into new stack
                       cut -2
                       deal with increment 7
                       cut 8
                       cut -4
                       deal with increment 7
                       cut 3
                       deal with increment 9
                       deal with increment 3
                       cut -1
                       """,
                       answer: [9, 2, 5, 8, 1, 4, 7, 0, 3, 6]
                       ))

// Run tests
for test in tests {
    let cards = Cards(10)
    cards.execute(test.instructions)
    if cards.deck == test.answer {
        print("Passed")
    } else {
        print("Failed!")
        print("Guessed: \(cards.deck)")
        print("Answer:  \(test.answer)")
    }
}

let inputFile = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input")
let input = try String.init(contentsOfFile: inputFile)

func part1() {
    let cards = Cards(10007)
    cards.execute(input)
    print(cards.deck.firstIndex(of: 2019) ?? "Not found")
}

part1()
