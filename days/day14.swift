#!/usr/bin/env swift

import Foundation

struct TestEntry {
    var input: String
    var answer: Int
    var answer2: Int
}

var testsPart1 = [TestEntry]()

testsPart1.append(TestEntry(
    input: """
    3 A, 4 B, 6 C => 1 FUEL
    10 ORE => 5 A
    6 A => 3 B
    3 A => 2 C
    """
    ,
    answer: 50,
    answer2: -1
    ))

testsPart1.append(TestEntry(
    input: """
    10 ORE => 10 A
    1 ORE => 1 B
    7 A, 1 B => 1 C
    7 A, 1 C => 1 D
    7 A, 1 D => 1 E
    7 A, 1 E => 1 FUEL
    """
    ,
    answer: 31,
    answer2: -1
    ))

testsPart1.append(TestEntry(
    input: """
    9 ORE => 2 A
    8 ORE => 3 B
    7 ORE => 5 C
    3 A, 4 B => 1 AB
    5 B, 7 C => 1 BC
    4 C, 1 A => 1 CA
    2 AB, 3 BC, 4 CA => 1 FUEL
    """
    ,
    answer: 165,
    answer2: -1
    ))

testsPart1.append(TestEntry(
    input: """
    157 ORE => 5 NZVS
    165 ORE => 6 DCFZ
    44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
    12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
    179 ORE => 7 PSHF
    177 ORE => 5 HKGWZ
    7 DCFZ, 7 PSHF => 2 XJWVT
    165 ORE => 2 GPVTF
    3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT
    """
    ,
    answer: 13312,
    answer2: 82892753
    ))

testsPart1.append(TestEntry(
    input: """
    2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
    17 NVRVD, 3 JNWZP => 8 VPVL
    53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
    22 VJHF, 37 MNCFX => 5 FWMGM
    139 ORE => 4 NVRVD
    144 ORE => 7 JNWZP
    5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
    5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
    145 ORE => 6 MNCFX
    1 NVRVD => 8 CXFTF
    1 VJHF, 6 MNCFX => 4 RFSQX
    176 ORE => 6 VJHF
    """
    ,
    answer: 180697,
    answer2: 5586022
    ))

testsPart1.append(TestEntry(
    input: """
    171 ORE => 8 CNZTR
    7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
    114 ORE => 4 BHXH
    14 VRPVC => 6 BMBT
    6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
    6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
    15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
    13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
    5 BMBT => 4 WPTQ
    189 ORE => 9 KTJDG
    1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
    12 VRPVC, 27 CNZTR => 2 XDBXC
    15 KTJDG, 12 BHXH => 5 XCVML
    3 BHXH, 2 VRPVC => 7 MZWV
    121 ORE => 7 VRPVC
    7 XCVML => 6 RJRHP
    5 BHXH, 4 VRPVC => 5 LTCX
    """
    ,
    answer: 2210736,
    answer2: 460664
    ))

// Make a dictionary with instructions on how to make something.
// eg, for the first example, we have:
// "7 A, 1 E => 1 FUEL"
// This should be a dictionary entry:
// "FUEL": (quantity: 1, requires: [(quantity: 7, chemical: "A"), ...])
// Then we can look up "A" in our dictionary to find out how to get at
// least 7 A.

struct Ingredient {
    var quantity: Int
    var chemical: String
}

struct RecipeEntry {
    var quantity: Int
    var ingredients: [Ingredient]
}

func processRecipe(from recipeS: String) -> [String: RecipeEntry] {
    var recipe: [String: RecipeEntry] = [:]

    for line in recipeS.components(separatedBy: "\n") {
        if line.count < 3 {
            // Invalid line
            continue
        }
        // Separate the ingredients and product
        let stuff = line.components(separatedBy: " => ")
        let ingredients = stuff[0].components(separatedBy: ", ")
        let product = stuff[1].components(separatedBy: " ")

        let name = product[1]

        guard let prodQuantity = Int(product[0]) else {
            continue
        }
        var entry = RecipeEntry(quantity: prodQuantity, ingredients: [])

        for chemical in ingredients {
            let item = chemical.components(separatedBy: " ")
            guard let itemQuantity = Int(item[0]) else {
                continue
            }
            let itemName = item[1]
            entry.ingredients.append(Ingredient(quantity: itemQuantity, chemical: itemName))
        }
        recipe[name] = entry
    }
    return recipe

}

func react(recipe: [String: RecipeEntry], target: String, neededQuantity: Int, storage: inout [String:Int], oreUsed: inout Int) {
    var amount = neededQuantity
    // print("Looking for \(amount) \(target)")
    // print(storage)

    // Get what we have stored first
    let remaining = storage[target, default: 0] - amount
    if remaining > 0 {
        storage[target, default: 0] = remaining
        return
    }
    // We've used up all that we had, keep getting more!
    amount -= storage[target, default: 0]
    storage[target] = 0

    // print(storage)

    // We need to manufacture the target!
    guard let need = recipe[target] else {
        // Should never happen!
        print("Cannot find \(target) in the recipe list!")
        return
    }

    // We need to do some processing.  We need *amount* but we can only get
    // it in increments of need.quantity.  Work out how many *lots we will
    // need
    let lotsD = ceil(Double(amount) / Double(need.quantity))
    let lots = Int(lotsD)

    // If all we require is ORE, then use that - no need to go recursive!
    if need.ingredients.first!.chemical == "ORE" {

        // We need *amount* of target.  However processing ORE will give us
        // need.quantity instead... so work out how many lots of
        // need.quantity we need to use up!
        storage[target] = lots * need.quantity
        oreUsed += lots * need.ingredients.first!.quantity

        // print("We processed ore to make \(storage[target]!) \(target)")
        // We now have some - just remove what we need from storage
        storage[target]! -= amount
        return
    }

    // We need other ingredients
    for demand in need.ingredients {
        react(recipe: recipe, target: demand.chemical, neededQuantity: lots * demand.quantity, storage: &storage, oreUsed: &oreUsed)
    }
    // Assume we got all we requested...
    storage[target, default: 0] += lots * need.quantity
    storage[target]! -= amount
    // print("We needed \(amount) \(target) and we got ",
    // "\(storage[target, default: 0]) which is \(lots) * \(need.quantity)")
}

func howMuchOreForFuel(from recipeS: String, fuelAmount: Int = 1) -> Int {

    let recipe = processRecipe(from: recipeS)

    // We are going to keep track of just how much ore we use separately
    var oreUsed = 0

    // We will need to keep track of the available chemicals we have...

    var storage = [String: Int]()

    react(recipe: recipe, target: "FUEL", neededQuantity: fuelAmount, storage: &storage, oreUsed: &oreUsed)

    /*
    for chems in storage.keys.sorted() where storage[chems, default: 0] > 0 {
        print("\(chems): \(storage[chems, default: -1])  ", terminator: "")
    }
    print()
    */
    return oreUsed
}

var passes = 0
for test in testsPart1 {
    let guess = howMuchOreForFuel(from: test.input)
    if guess == test.answer {
        passes += 1
        print("Passed test \(passes)")
    } else {
        print(test.input)
        print("Got \(guess) instead of \(test.answer)")
    }
}
print("Passed \(passes) out of \(testsPart1.count)")

if passes == testsPart1.count {

    if CommandLine.argc == 2 {
        let input = try String.init(contentsOfFile: CommandLine.arguments[1])
        print("Guess for Day 14 part 1 is \(howMuchOreForFuel(from: input))")
    }
}

if CommandLine.argc == 2 {
    let input = try String.init(contentsOfFile: CommandLine.arguments[1])
    testsPart1.append(TestEntry(input: input, answer: 10, answer2: 10))
}

// Try to determine how much fuel we'll get from 1000000000000 ores
for test in testsPart1 where test.answer2 > 0 {

    let targetOres = 1000000000000
    let ores = howMuchOreForFuel(from: test.input)
    print("Expecting an answer of \(test.answer2)")

    // Set an upper and lower bound
    let upperBound = Int(Double(targetOres) * 20) / ores
    let lowerBound = Int(Double(targetOres) * 0.5) / ores

    precondition(howMuchOreForFuel(from: test.input,
                                   fuelAmount: upperBound) > targetOres,
    "Failed to get a good upperBound")
    precondition(howMuchOreForFuel(from: test.input,
                                   fuelAmount: lowerBound) < targetOres,
    "Failed to get a good lowerBound")

    var low = lowerBound
    var high = upperBound
    var count = 0
    while high - low > 1 {
        count += 1
        let mid = low + (high - low) / 2
        if howMuchOreForFuel(from: test.input, fuelAmount: mid) < targetOres {
            low = mid + 1
        } else {
            high = mid
        }
    }
    if low == test.answer2 {
        print("Passed")
    } else {
        print("The guess is \(low)")
    }
}
/*
for check in 3..<testsPart1.count {
    print("Investigating \(testsPart1[check].answer)")
    let (fuel, ores) = howMuchOreWithoutWaste(from: testsPart1[check].input)

    print("We get \(fuel) fuel for \(ores) ores without any waste")
    print("Which means we should get ",
          "\(Int(Double(fuel) * (Double(1000000000000) / Double(ores))) ) ",
          "fuel for 1 trillion ores")
}

*/
