#!/usr/bin/env swift

import Foundation

func makeLayers(width: Int, height: Int, input: String) -> [[Int]] {

    // Convert the data into a list of integers
    var data: [Int] = []
    input.forEach {
        if let i = Int(String($0)) {
            data.append(i)
        }
    }
    let layerSize = width * height

    var layers:[[Int]] = []

    var layer: [Int] = []
    for i in data {
        if layer.count < layerSize {
            layer.append(i)
        } else {
            // We need to make a new layer after adding this to our list of layers
            layers.append(layer)
            layer = []
            layer.append(i)
        }
    }
    // There may be a leftover layer
    if layer.count > 0 {
        layers.append(layer)
    }
    return layers
}

func part1(width: Int, height: Int, data: String) -> Int {

    let layers = makeLayers(width: width, height: height, input: data)

    var zeroes = width * height // The maximum possible
    var lowestLayer: [Int] = []
    for l in layers {
        let newzeroes = l.filter{$0 == 0}.count
        if newzeroes < zeroes {
            zeroes = newzeroes
            lowestLayer = l
        }
    }
    // We need to return the count of 1 digits * count of 2 digits
    // return lowestLayer.filter{$0 == 1}.count * lowestLayer.filter{$0 == 2}.count
    // That was old-fashioned.  Let's use a bit of Swift-5 magic!
    let dictionary = lowestLayer.reduce(into: [:]) {
        counts, number in counts[number, default: 0] += 1
    }
    return dictionary[1]! * dictionary[2]!
}

func part2(width: Int, height: Int, data: String) -> String {

    let layers = makeLayers(width: width, height: height, input: data)

    var image: [Int] = []

    // To compose our image, we need to go through each layer, calculating the
    // right pixel value
    for i in 0..<width*height {
        var pixel: Int = 2 // Transparent by default
        for l in layers {
            let lp = l[i]
            if pixel == 2 {
                pixel = lp
            }
        }
        image.append(pixel)
    }
    var message = ""
    for pos in 0..<image.count {
        if pos%width == 0 {
            message.append("\n")
        }
        let p = image[pos]
        if p == 0 {
            message.append(" ")
        } else {
            message.append("X")
        }
    }

    return message
}

let testSuite1 = (input: "123456789012", answer: 1)

if part1(width: 3, height: 2, data: testSuite1.input) == testSuite1.answer {
    if CommandLine.argc == 2 {
        let input = try String.init(contentsOfFile: CommandLine.arguments[1])
        
        print("Part1: \(part1(width: 25, height: 6, data:input))")
    }
}

let input2 = "0222112222120000"
print(part2(width: 2, height: 2, data: input2))

if CommandLine.argc == 2 {
    let input = try String.init(contentsOfFile: CommandLine.arguments[1])

    print(part2(width: 25, height: 6, data: input))
}
