#!/usr/bin/env swift

import Foundation

// For day 23 we need a couple of small changes:
// * As they are running in a network, each output is now 3 digits (addr, x, y)
// * If there is no input, -1 should be provided
class IntCode {
    var halted: Bool
    var memory: [Int: Int] = [:]
    var input: [Int]
    var output: [Int]
    var progCounter: Int
    var relBase: Int
    var starved: Bool
    init(_ code: [Int], input: [Int]? = nil) {
        self.halted = false // We halt when we hit 99
        if let inp = input {
            self.input = inp
        } else {
            self.input = []
        }
        self.starved = false
        self.progCounter = 0
        self.relBase = 0
        self.output = []
        // Put the code into memory
        for ind in code.indices {
            self.memory[ind] = code[ind]
        }
    }
    func runTilInput(_ input: [Int]? = nil) -> [Int]? {
        if self.halted {
            print("Halted")
            return nil
        }
        if let passedIn = input {
            self.input += passedIn
            self.starved = false
        }
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            var codestr = String(self.memory[self.progCounter, default: 0])
            let opstr = codestr.suffix(2)
            let opcode = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.progCounter += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            for i in 0..<3 {
                var paramType = 0
                if codestr.count > 0 {
                    paramType = Int(String(codestr.popLast() ?? "0"))!
                }
                switch paramType {
                case 0:
                    // Positional
                    paramPos.append(self.memory[self.progCounter + i, default: 0])
                case 1:
                    // Immediate
                    paramPos.append(self.progCounter + i)
                case 2:
                    // Relative
                    // ie relBase + what is in this memory location
                    paramPos.append(self.relBase + self.memory[self.progCounter + i, default: 0])
                default:
                    print("Unrecognised parameter type \(paramType)")
                    return nil
                }
            }
            // print(opcode, paramPos)
            switch opcode {
            case 99:
                self.halted = true
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 + in2
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 * in2
            case 3:
                // If we don't have input, just return and hopefully by the
                // next round we'll have some more!
                if self.input.count < 1 {
                    self.starved = true
                    self.progCounter -= 1
                    self.memory[paramPos[0]] = -1
                    return nil
                }
                self.memory[paramPos[0]] = self.input.removeFirst()
                self.progCounter += 1
            case 4:
                // Get output
                self.output.append(self.memory[paramPos[0], default: 0])
                self.progCounter += 1
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0], default: 0] < self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 8:
                // equals
                if self.memory[paramPos[0], default: 0] == self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 9:
                // Adjust relative base
                self.relBase += self.memory[paramPos[0], default: 0]
                self.progCounter += 1
            default:
                print("Unrecognised opcode \(opcode)")
                return nil
            }
            return nil
        }
    }
}

var code: [Int] = []
let inputFile = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input")
let input = try String.init(contentsOfFile: inputFile)
.components(separatedBy: "\n")[0]
for char in input.components(separatedBy: ",") {
    if let int = Int(char) {
        code.append(int)
    }
}

func part1() {
    var network: [IntCode] = []

    var incoming: [[Int]] = []
    var donePart1 = false

    var natStore: [Int] = []

    var sentYToZero = -1

    for address in 0..<50 {
        let machine = IntCode(code, input: [address, -1])
        network.append(machine)
        incoming.append([])
    }

    // Now that we have set up the network, keep running, until we find the
    // elusive Y value to address 255

    var iteration = 0
    while true {
        iteration += 1
        // Check for idleness
        var idle = true
        for machine in network where !machine.starved {
            idle = false
        }
        for msg in incoming where msg.count > 0 {
            idle = false
        }
        if idle && natStore.count > 1 {
            // The network is idle!  Send the nat store to machine 0
            incoming[0] = natStore
            if natStore[1] == sentYToZero {
                print(" Part 2: \(sentYToZero)")
                return
            } else {
                sentYToZero = natStore[1]
            }
        }
        for address in 0..<network.count {
            let input = incoming[address]
            incoming[address] = [Int]()

            // We will ignore errors - obviously not production code!
            _ = network[address].runTilInput(input)
            // Check if we have any completed network packets to send on.
            while network[address].output.count > 2 {
                // Note that we're not checking the protocol properly!
                let receiver = network[address].output.removeFirst()
                let valX = network[address].output.removeFirst()
                let valY = network[address].output.removeFirst()

                if receiver == 255 {
                    if !donePart1 {
                        print("\n Part 1: \(valY)")
                        print("Part 2 will be here within a minute :-)")
                    }
                    donePart1 = true
                    natStore = [valX, valY]
                    continue
                }
                if receiver < 0 || receiver >= network.count {
                    print("Unexpected recepient: \(receiver)")
                    continue
                }
                incoming[receiver] += [valX, valY]
            }
        }
    }
}

part1()
