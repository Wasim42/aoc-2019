#!/usr/bin/env swift

import Foundation

struct Xyz: CustomStringConvertible {
    var x: Int
    var y: Int
    var z: Int
    var description: String {
        return "<x=\(String(format: "% 3d", x)), y=\(String(format: "% 3d", y)), z=\(String(format: "% 3d", z))>"
    }
    var energy: Int { return Int(x.magnitude + y.magnitude + z.magnitude) }
}

extension Xyz: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
        hasher.combine(z)
    }
}

struct Moon {
    var pos: Xyz
    var vel: Xyz
    var energy: Int { return pos.energy * vel.energy }
}

extension Moon: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(pos)
        hasher.combine(vel)
    }
}

enum Moons: Int, CaseIterable {
    case io = 1, europa, ganymede, callisto
}

class MoonsOfJupiter: CustomStringConvertible {
    var moons: [Moons:Moon]
    var description: String {
        var msg = ""
        for (_, m) in moons {
            msg.append("pos=\(m.pos), vel=\(m.vel)\n")
        }
        return msg
    }
    var energy: Int {
        var e = 0
        for (_, m) in moons {
            e += m.energy
        }
        return e
    }


    init(ix: Int, iy: Int, iz: Int, ex: Int, ey: Int, ez: Int, gx: Int, gy: Int, gz: Int, cx: Int, cy: Int, cz: Int) {
        let initvel = Xyz(x:0, y:0, z:0)
        let ixyz = Xyz(x:ix, y:iy, z:iz)
        let exyz = Xyz(x:ex, y:ey, z:ez)
        let gxyz = Xyz(x:gx, y:gy, z:gz)
        let cxyz = Xyz(x:cx, y:cy, z:cz)

        moons = [:]
        moons[.io] = Moon(pos: ixyz, vel: initvel)
        moons[.europa] = Moon(pos: exyz, vel: initvel)
        moons[.ganymede] = Moon(pos: gxyz, vel: initvel)
        moons[.callisto] = Moon(pos: cxyz, vel: initvel)
    }

    func applySteps(_ count: Int = 1) {
        for _ in 0..<count {
            for ma in Moons.allCases {
                for mb in Moons.allCases {
                    if ma.rawValue <= mb.rawValue {
                        // Alread covered, otherwise it's us!
                        continue
                    }
                    // applyGravity takes an index, not the moon itself
                    self.applyGravity(ma, mb)
                }
            }
            for m in Moons.allCases {
                // applyGravity takes an index, not the moon itself
                self.applyVelocity(m)
            }
        }
    }

    func applyGravity(_ ma: Moons, _ mb: Moons) {
        guard let aa = moons[ma] else {
            return
        }
        guard let bb = moons[mb] else {
            return
        }
        var a = aa
        var b = bb
        // TODO: There must be a nicer way to do this
        if a.pos.x < b.pos.x {
            a.vel.x += 1
            b.vel.x -= 1
        } else if a.pos.x > b.pos.x {
            a.vel.x -= 1
            b.vel.x += 1
        }
        if a.pos.y < b.pos.y {
            a.vel.y += 1
            b.vel.y -= 1
        } else if a.pos.y > b.pos.y {
            a.vel.y -= 1
            b.vel.y += 1
        }
        if a.pos.z < b.pos.z {
            a.vel.z += 1
            b.vel.z -= 1
        } else if a.pos.z > b.pos.z {
            a.vel.z -= 1
            b.vel.z += 1
        }

        // Put the moons back in as we applied it to a copy
        moons[ma] = a
        moons[mb] = b
        
    }
    func applyVelocity(_ mm: Moons) {
        guard let moon = moons[mm] else {
            return
        }
        var m = moon
        m.pos.x += m.vel.x
        m.pos.y += m.vel.y
        m.pos.z += m.vel.z
        moons[mm] = m
    }



}

let example1 = MoonsOfJupiter(ix:-1,iy:0,iz:2,ex:2,ey:-10,ez:-7,gx:4,gy:-8,gz:8,cx:3,cy:5,cz:-1)

print(example1)
example1.applySteps(10)
print(example1)
if example1.energy != 179 {
    print("Failed example 1")
} else {
    print("Passed example 1")
}

let example2 = MoonsOfJupiter(ix:-8,iy:-10,iz:0,ex:5,ey:5,ez:10,gx:2,gy:-7,gz:3,cx:9,cy:-8,cz:-3)
print(example2)
example2.applySteps(100)
if example2.energy != 1940 {
    print("Failed example 2")
} else {
    print("Passed example 2")
}

let test = MoonsOfJupiter(ix:-1,iy:7,iz:3,ex:12,ey:2,ez:-13,gx:14,gy:18,gz:-8,cx:17,cy:4,cz:-4)
print("Day 12 Part 1:")
print(test)
test.applySteps(1000)
print("Total energy for test after 100 steps: \(test.energy)")

// Part 2
func gcd(_ inA: Int, _ inB: Int) -> Int {

    var a = inA
    var b = inB

    while b != 0 {
        let t = b
        b = a % b
        a = t
    }
    return a
}

func lcm(_ inA: Int, _ inB: Int) -> Int {
    return (inA * inB) / gcd(inA, inB)
}

// My suspicion is that each of the parameters exhibits a periodicity to which
// we need to find a common factor-type thing

// Given that we start at an initial value, we will probably repeat to that
// initial value too

// The maths tells us that the X, Y, and Z values are completely independent.
// So we just need to find the periodicity of each, and we should be fine (for
// this simplified test - I'm sure the values have been chosen to make this
// work)

struct MoonVals {
    var ipx: Int
    var ivx: Int
    var epx: Int
    var evx: Int
    var gpx: Int
    var gvx: Int
    var cpx: Int
    var cvx: Int
}

extension MoonVals: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(ipx)
        hasher.combine(ivx)
        hasher.combine(epx)
        hasher.combine(evx)
        hasher.combine(gpx)
        hasher.combine(gvx)
        hasher.combine(cpx)
        hasher.combine(cvx)
    }
}

func timeToRepeat(orbits: MoonsOfJupiter) -> Int {

    // The 'x' values are independent of the 'y' values, which are independent
    // of the 'z' values

    // Find the periodicity of the x, y, and z values of all the moons,
    // independently

    let io = orbits.moons[Moons.io]!
    let europa = orbits.moons[Moons.europa]!
    let ganymede = orbits.moons[Moons.ganymede]!
    let callisto = orbits.moons[Moons.callisto]!

    let initX = MoonVals(ipx: io.pos.x, ivx: io.vel.x,
                         epx: europa.pos.x, evx: europa.vel.x,
                         gpx: ganymede.pos.x, gvx: ganymede.vel.x,
                         cpx: callisto.pos.x, cvx: callisto.vel.x)
    var periodX = 0
    var lastSeenX = 0
    let initY = MoonVals(ipx: io.pos.y, ivx: io.vel.y,
                         epx: europa.pos.y, evx: europa.vel.y,
                         gpx: ganymede.pos.y, gvx: ganymede.vel.y,
                         cpx: callisto.pos.y, cvx: callisto.vel.y)
    var periodY = 0
    var lastSeenY = 0
    let initZ = MoonVals(ipx: io.pos.z, ivx: io.vel.z,
                         epx: europa.pos.z, evx: europa.vel.z,
                         gpx: ganymede.pos.z, gvx: ganymede.vel.z,
                         cpx: callisto.pos.z, cvx: callisto.vel.z)
    var periodZ = 0
    var lastSeenZ = 0

    var step = 0
    while periodX == 0 || periodY == 0 || periodZ == 0 {
        orbits.applySteps()
        step += 1

        // This is just to make the compiler happy!
        guard let io = orbits.moons[Moons.io] else {
            continue
        }
        guard let europa = orbits.moons[Moons.europa] else {
            continue
        }
        guard let ganymede = orbits.moons[Moons.ganymede] else {
            continue
        }
        guard let callisto = orbits.moons[Moons.callisto] else {
            continue
        }
        if periodX == 0 {
            let xs = MoonVals(ipx: io.pos.x, ivx: io.vel.x,
                              epx: europa.pos.x, evx: europa.vel.x,
                              gpx: ganymede.pos.x, gvx: ganymede.vel.x,
                              cpx: callisto.pos.x, cvx: callisto.vel.x)
            if xs == initX {
                periodX = step - lastSeenX
                lastSeenX = step
            }
        }
        if periodY == 0 {
            let ys = MoonVals(ipx: io.pos.y, ivx: io.vel.y,
                              epx: europa.pos.y, evx: europa.vel.y,
                              gpx: ganymede.pos.y, gvx: ganymede.vel.y,
                              cpx: callisto.pos.y, cvx: callisto.vel.y)
            if ys == initY {
                periodY = step - lastSeenY
                lastSeenY = step
            }
        }
        if periodZ == 0 {
            let zs = MoonVals(ipx: io.pos.z, ivx: io.vel.z,
                              epx: europa.pos.z, evx: europa.vel.z,
                              gpx: ganymede.pos.z, gvx: ganymede.vel.z,
                              cpx: callisto.pos.z, cvx: callisto.vel.z)
            if zs == initZ {
                periodZ = step - lastSeenZ
                lastSeenZ = step
            }
        }
    }

    return lcm(periodX, lcm(periodY, periodZ))
}

let example3 = MoonsOfJupiter(ix:-1,iy:0,iz:2,ex:2,ey:-10,ez:-7,gx:4,gy:-8,gz:8,cx:3,cy:5,cz:-1)

print(timeToRepeat(orbits: example3))

let example4 = MoonsOfJupiter(ix:-8,iy:-10,iz:0,ex:5,ey:5,ez:10,gx:2,gy:-7,gz:3,cx:9,cy:-8,cz:-3)
print(timeToRepeat(orbits: example4))


let part2 = MoonsOfJupiter(ix:-1,iy:7,iz:3,ex:12,ey:2,ez:-13,gx:14,gy:18,gz:-8,cx:17,cy:4,cz:-4)

print("Part 2 answer: \(timeToRepeat(orbits: part2))")
