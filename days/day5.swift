#!/usr/bin/env swift

import Foundation

// Test driven programming... set up the tests first :-)

struct TestEntry {
    var code: [Int]
    var input: Int
    var answer: Int
}

var testSuite: [TestEntry] = []
testSuite.append(TestEntry(code: [3,9,8,9,10,9,4,9,99,-1,8], input: 3, answer: 0))
testSuite.append(TestEntry(code: [3,9,8,9,10,9,4,9,99,-1,8], input: 8, answer: 1))
testSuite.append(TestEntry(code: [3,9,7,9,10,9,4,9,99,-1,8], input: 3, answer: 1))
testSuite.append(TestEntry(code: [3,9,7,9,10,9,4,9,99,-1,8], input: 9, answer: 0))
testSuite.append(TestEntry(code: [3,3,1108,-1,8,3,4,3,99], input: 9, answer: 0))
testSuite.append(TestEntry(code: [3,3,1108,-1,8,3,4,3,99], input: 8, answer: 1))
testSuite.append(TestEntry(code: [3,3,1107,-1,8,3,4,3,99], input: 8, answer: 0))
testSuite.append(TestEntry(code: [3,3,1107,-1,8,3,4,3,99], input: 7, answer: 1))
testSuite.append(TestEntry(code: [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input: 7, answer: 1))
testSuite.append(TestEntry(code: [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input: 0, answer: 0))
testSuite.append(TestEntry(code: [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input: 0, answer: 0))
testSuite.append(TestEntry(code: [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input: 7, answer: 1))
testSuite.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: 7, answer: 999))
testSuite.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: 8, answer: 1000))
testSuite.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: 9, answer: 1001))

func intCode(code: inout [Int], input: Int) -> Int? {

    var pos = 0
    var output = 0
    while true {
        // The first number is an opcode, and potentially a set of operating modes
        var codestr = String(code[pos])
        let opstr = codestr.suffix(2)
        let op = Int(opstr) ?? 0
        // Now remove the opcode
        codestr.removeLast()
        // It's possible that it's a single-digit opcode...
        if codestr.count > 0 {
            codestr.removeLast()
        }
        // WARNING - we have incremented the program counter
        pos += 1
        // We have potentially three parameters.  They are either positional,
        // or immediate parameters.
        var paramPos: [Int] = []
        // TODO: This is an ugly hacky mess atm...
        // Opcodes 1,2,3,4,5,6,7,8 have at least 1 parameter
        if op>=0 && op<=8 {
            paramPos.append(code[pos])
        }
        // Opcodes 1,2 5,6,7,8 have at least 2 parameters
        if (op>=1 && op<=2) || (op>=5 && op<=8) {
            paramPos.append(code[pos+1])
        }
        // Opcodes 1,2 7,8 have the full 3 parameters
        if (op>=1 && op<=2) || (op>=7 && op<=8) {
            paramPos.append(code[pos+2])
        }
        // The above has assumed positional arguments (0)
        // Now convert to immediate where appropriate
        for i in 0..<codestr.count {
            if codestr.popLast() ?? "0" == "1" {
                paramPos[i] = pos + i
            }
        }
        switch op {
        case 99:
            return output
        case 1:
            // Add
            let in1 = code[paramPos[0]]
            let in2 = code[paramPos[1]]
            let outpos = paramPos[2]
            pos += 3
            code[outpos] = in1 + in2
            continue
        case 2:
            // Multiply
            let in1 = code[paramPos[0]]
            let in2 = code[paramPos[1]]
            let outpos = paramPos[2]
            pos += 3
            code[outpos] = in1 * in2
            continue
        case 3:
            // Store input
            code[paramPos[0]] = input
            pos += 1
        case 4:
            // Get output
            output = code[paramPos[0]]
            pos += 1
        case 5:
            // jump-if-true
            if code[paramPos[0]] != 0 {
                pos = code[paramPos[1]]
            } else {
                pos += 2
            }
        case 6:
            // jump-if-false
            if code[paramPos[0]] == 0 {
                pos = code[paramPos[1]]
            } else {
                pos += 2
            }
        case 7:
            // less than
            if code[paramPos[0]] < code[paramPos[1]] {
                code[paramPos[2]] = 1
            } else {
                code[paramPos[2]] = 0
            }
            pos += 3
        case 8:
            // equals
            if code[paramPos[0]] == code[paramPos[1]] {
                code[paramPos[2]] = 1
            } else {
                code[paramPos[2]] = 0
            }
            pos += 3
        default:
            print("Unrecognised opcode \(op)")
            return nil
        }
    }
}

func day5() -> [Int]? {
    do {
        var numbers: [String]

        if CommandLine.argc > 2 ||
        CommandLine.argc > 1 && CommandLine.arguments[1].hasPrefix("-") {
            // The equivalent of --help
            print("\(CommandLine.arguments[0]) [filename]")
            print("\n   If a filename is not provided, we read from stdin")
            return nil
        } else if CommandLine.argc == 2 {
            // We have been given the filename to use
            let nums: [String] = try String.init(contentsOfFile: CommandLine.arguments[1])
            .components(separatedBy: "\n")
            numbers = nums
        } else {
            // Read from stdin continually until no more input.
            print("Please enter your input - blank line to finish:")
            numbers = []
            while true {
                if let line = readLine() {
                    if line.count < 1 {
                        // Blank line - finish
                        break
                    }
                    numbers.append(line)
                } else {
                    // ctrl-d - finish
                    break
                }
            }
        }

        // This is a long string... so we need to convert it to an array of int's
        var input: [Int] = []
        let strings = numbers[0].components(separatedBy: ",")
        for s in strings {
            if let i = Int(s) {
                input.append(i)
            }
        }

        var input1 = input
        var input2 = input
        if let answer1 = intCode(code: &input1, input: 1) {
            if let answer2 = intCode(code: &input2, input: 5) {
                return [answer1, answer2]
            }
        }
    } catch {
        print("Sorry - I failed to read \(CommandLine.arguments[1])!")
    }
    return nil
}

// Run some tests

var passes = 0
for t in testSuite {
    var code = t.code
    if intCode(code: &code, input: t.input) != t.answer {
        print("Failed testSuite \(t)")
    } else {
        print("Passed \(passes)")
        passes += 1
    }
}
if passes == testSuite.count {
    print("100% pass!")
    print(day5() ?? "Failed day 5")
} else {
    print("You have more fixes to do!")
}
