#!/usr/bin/env swift

import Foundation

// Test driven programming... set up the tests first :-)

struct TestEntry {
    var code: [Int]
    var input: [Int]
    var answer: [Int]
}

var regressionTests: [TestEntry] = []
regressionTests.append(TestEntry(code: [3,9,8,9,10,9,4,9,99,-1,8], input: [3], answer: [0]))
regressionTests.append(TestEntry(code: [3,9,8,9,10,9,4,9,99,-1,8], input: [8], answer: [1]))
regressionTests.append(TestEntry(code: [3,9,7,9,10,9,4,9,99,-1,8], input: [3], answer: [1]))
regressionTests.append(TestEntry(code: [3,9,7,9,10,9,4,9,99,-1,8], input: [9], answer: [0]))
regressionTests.append(TestEntry(code: [3,3,1108,-1,8,3,4,3,99], input: [9], answer: [0]))
regressionTests.append(TestEntry(code: [3,3,1108,-1,8,3,4,3,99], input: [8], answer: [1]))
regressionTests.append(TestEntry(code: [3,3,1107,-1,8,3,4,3,99], input: [8], answer: [0]))
regressionTests.append(TestEntry(code: [3,3,1107,-1,8,3,4,3,99], input: [7], answer: [1]))
regressionTests.append(TestEntry(code: [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input: [7], answer: [1]))
regressionTests.append(TestEntry(code: [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], input: [0], answer: [0]))
regressionTests.append(TestEntry(code: [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input: [0], answer: [0]))
regressionTests.append(TestEntry(code: [3,3,1105,-1,9,1101,0,0,12,4,12,99,1], input: [7], answer: [1]))
regressionTests.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: [7], answer: [999]))
regressionTests.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: [8], answer: [1000]))
regressionTests.append(TestEntry(
    code: [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
           1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
           999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], input: [9], answer: [1001]))

class IntCode {
    var halted: Bool
    var memory: [Int:Int] = [:]
    var input: Int
    var output: Int
    var pc: Int
    var relBase: Int
    var initInput: Bool
    init(code: [Int], input: Int) {
        self.halted = false // We halt when we hit 99
        self.input = input
        self.initInput = false
        self.pc = 0
        self.relBase = 0
        self.output = 0
        // Put the code into memory
        for i in code.indices {
            self.memory[i] = code[i]
        }
    }
    func run(_ input: Int) -> Int? {
        if self.halted {
            return nil
        }
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            /*
            var pc = 0
            while true {
                if let value = self.memory[pc] {
                    print("\(pc):\(value)", terminator: " ")
                    pc += 1
                } else {
                    break
                }
            }
            print()
            */
            var codestr = String(self.memory[self.pc, default: 0])
            let opstr = codestr.suffix(2)
            let op = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.pc += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            for i in 0..<3 {
                var paramType = 0
                if codestr.count > 0 {
                    paramType = Int(String(codestr.popLast() ?? "0"))!
                }
                switch paramType {
                case 0:
                    // Positional
                    paramPos.append(self.memory[self.pc + i, default: 0])
                case 1:
                    // Immediate
                    paramPos.append(self.pc + i)
                case 2:
                    // Relative
                    // ie relBase + what is in this memory location
                    paramPos.append(self.relBase + self.memory[self.pc + i, default: 0])
                default:
                    print("Unrecognised parameter type \(paramType)")
                    return nil
                }
            }
            // print(op, paramPos)
            switch op {
            case 99:
                self.halted = true
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.pc += 3
                self.memory[outpos] = in1 + in2
                continue
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.pc += 3
                self.memory[outpos] = in1 * in2
                continue
            case 3:
                // Pass in the current input
                if self.initInput {
                    self.memory[paramPos[0]] = input
                } else {
                    self.memory[paramPos[0]] = self.input
                    self.initInput = true
                }
                self.pc += 1
            case 4:
                // Get output
                self.output = self.memory[paramPos[0], default: 0]
                self.pc += 1
                return self.output
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.pc = self.memory[paramPos[1], default: 0]
                } else {
                    self.pc += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.pc = self.memory[paramPos[1], default: 0]
                } else {
                    self.pc += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0], default: 0] < self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.pc += 3
            case 8:
                // equals
                if self.memory[paramPos[0], default: 0] == self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.pc += 3
            case 9:
                // Adjust relative base
                self.relBase += self.memory[paramPos[0], default: 0]
                self.pc += 1
            default:
                print("Unrecognised opcode \(op)")
                return nil
            }
        }
    }
}

var code: [Int] = []
if CommandLine.argc == 2 {
    let input = try String.init(contentsOfFile: CommandLine.arguments[1])
    .components(separatedBy: "\n")[0]
    for n in input.components(separatedBy: ",") {
        if let i = Int(n) {
            code.append(i)
        }
    }
}

func runToCompletion(code: [Int], input: Int = 0) -> [Int]{
    var outputs: [Int] = []

    let machine = IntCode(code: code, input: input)

    while !machine.halted {
        if let output = machine.run(input) {
            outputs.append(output)
        }
    }
    return outputs
}

// Run some tests
var passes = 0

// Check the regression tests first
for t in regressionTests {
    let code = t.code
    let guess = runToCompletion(code: code, input: t.input[0])
    if guess != t.answer {
        print("Wrong guess \(guess) in test \(t)")
    } else {
        passes += 1
        print("Passed regression test \(passes)")
    }
}

var day9Tests: [TestEntry] = []
day9Tests.append(TestEntry(code: [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99], input: [], answer: [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]))
day9Tests.append(TestEntry(code: [1102,34915192,34915192,7,4,7,99,0], input: [], answer: [1219070632396864]))
day9Tests.append(TestEntry(code: [104,1125899906842624,99], input: [], answer: [1125899906842624]))

passes = 0

for t in day9Tests {
    let guess = runToCompletion(code: t.code, input: 0)
    if guess != t.answer {
        print("Wrong guess \(guess) in test \(t)")
    } else {
        passes += 1
        print("Passed day-9 test \(passes)")
    }
}

if passes == day9Tests.count {
    let part1 = runToCompletion(code: code, input: 1)
    print("Part1: \(part1)")
    let part2 = runToCompletion(code: code, input: 2)
    print("Part2: \(part2)")
}
