#!/usr/bin/env swift

import Foundation

// Note that this does not use a queue for input unlike the other days!
// Hence the lack of any regression tests
class IntCode {
    var halted: Bool
    var memory: [Int: Int] = [:]
    var input: [Int]
    var output: Int
    var progCounter: Int
    var relBase: Int
    init(_ code: [Int], input: [Int]? = nil) {
        self.halted = false // We halt when we hit 99
        if let inp = input {
            self.input = inp
        } else {
            self.input = []
        }
        self.progCounter = 0
        self.relBase = 0
        self.output = 0
        // Put the code into memory
        for ind in code.indices {
            self.memory[ind] = code[ind]
        }
    }
    func run(_ input: Int? = nil) -> Int? {
        if self.halted {
            print("Halted")
            return nil
        }
        if let passedIn = input {
            self.input.append(passedIn)
        }
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            /*
            var pc = 0
            while true {
                if let value = self.memory[pc] {
                    print("\(pc):\(value)", terminator: " ")
                    pc += 1
                } else {
                    break
                }
            }
            print()
            */
            var codestr = String(self.memory[self.progCounter, default: 0])
            let opstr = codestr.suffix(2)
            let opcode = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.progCounter += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            for i in 0..<3 {
                var paramType = 0
                if codestr.count > 0 {
                    paramType = Int(String(codestr.popLast() ?? "0"))!
                }
                switch paramType {
                case 0:
                    // Positional
                    paramPos.append(self.memory[self.progCounter + i, default: 0])
                case 1:
                    // Immediate
                    paramPos.append(self.progCounter + i)
                case 2:
                    // Relative
                    // ie relBase + what is in this memory location
                    paramPos.append(self.relBase + self.memory[self.progCounter + i, default: 0])
                default:
                    print("Unrecognised parameter type \(paramType)")
                    return nil
                }
            }
            // print(opcode, paramPos)
            switch opcode {
            case 99:
                print("We still have \(self.input.count) inputs to read")
                self.halted = true
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 + in2
                continue
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 * in2
                continue
            case 3:
                // Pass in the current input
                self.memory[paramPos[0]] = self.input.removeFirst()
                self.progCounter += 1
            case 4:
                // Get output
                self.output = self.memory[paramPos[0], default: 0]
                self.progCounter += 1
                return self.output
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0], default: 0] < self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 8:
                // equals
                if self.memory[paramPos[0], default: 0] == self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 9:
                // Adjust relative base
                self.relBase += self.memory[paramPos[0], default: 0]
                self.progCounter += 1
            default:
                print("Unrecognised opcode \(opcode)")
                return nil
            }
        }
    }
}

func runToCompletion(_ code: [Int], input: [Int]? = nil) -> [Int] {
    var outputs: [Int] = []

    let machine = IntCode(code, input: input)

    while !machine.halted {
        if let output = machine.run() {
            outputs.append(output)
        }
    }
    return outputs
}

var code: [Int] = []
let inputFile = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input")
let input = try String.init(contentsOfFile: inputFile)
.components(separatedBy: "\n")[0]
for char in input.components(separatedBy: ",") {
    if let int = Int(char) {
        code.append(int)
    }
}

enum Motion: Int, CaseIterable {
    case north = 1, east = 4, south = 2, west = 3
}

func opposite(_ motion: Motion) -> Motion {
    switch motion {
    case .north:
        return .south
    case .south:
        return .north
    case .east:
        return .west
    case .west:
        return .east
    }
}

struct CoOrds {
    // swiftlint:disable identifier_name
    var x: Int
    var y: Int
    // swiftlint:enable identifier_name
    mutating func move(_ motion: Motion) {
        switch motion {
        case .north:
            y += 1
        case .south:
            y -= 1
        case .east:
            x += 1
        case .west:
            x -= 1
        }
    }
}

extension CoOrds: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}

enum Item: UInt8, CaseIterable {
    case scaffold = 35
    // swiftlint:disable identifier_name
    case up = 94
    // swiftlint:enable identifier_name
    case down = 118
    case left = 60
    case right = 62
    case free = 46
    case lost = 88
    case newLine = 10
}

func buildAtlas(from values: [Int]) -> ([CoOrds: Item], CoOrds) {
    // Build an atlas. For part 1 we only care about the scaffolding, and
    // newlines.
    var atlas: [CoOrds: Item] = [:]

    var posX = -1, posY = 0
    var maxX = 0, maxY = 0 // Only used for visualisation
    for value in values {
        if value > 255 {
            print("\nWe got a value \(value)")
            continue
        }
        let char = Item(rawValue: UInt8(Int8(value)))
        posX += 1
        if posX > maxX {
            maxX = posX
        }
        switch char {
        case .newLine:
            posX = -1
            posY += 1
            maxY = posY
        case .scaffold, .up, .down, .left, .right, .free, .lost:
            atlas[CoOrds(x: posX, y: posY)] = char
        default:
            print(Character(UnicodeScalar(value)!) ?? " ", terminator: "")
        }
    }
    return (atlas, CoOrds(x: maxX, y: maxY))
}

func makeAtlas() -> ([CoOrds: Item], CoOrds) {
    // Returns the atlas, and the maximum x, y co-ordinates
    let values = runToCompletion(code)

    return buildAtlas(from: values)
}

func drawAtlas(_ atlas: [CoOrds: Item], maxXY: CoOrds) -> Int {
    // We return an alignment parameter
    var alignmentParam = 0
    // Draw the map and find the intersections
    for drawY in 0...maxXY.y {
        for drawX in 0...maxXY.x {
            if let item = atlas[CoOrds(x: drawX, y: drawY)] {
                if item == .scaffold {
                    // Check for an intersection
                    //          y-1
                    //      x-1  x  x+1
                    //          y+1
                    if atlas[CoOrds(x: drawX-1, y: drawY)] == .scaffold &&
                    atlas[CoOrds(x: drawX+1, y: drawY)] == .scaffold &&
                    atlas[CoOrds(x: drawX, y: drawY-1)] == .scaffold &&
                    atlas[CoOrds(x: drawX, y: drawY+1)] == .scaffold {
                        print("O", terminator: "")
                        alignmentParam += drawX*drawY
                    } else {
                        print("#", terminator: "")
                    }
                } else {
                    print(Character(UnicodeScalar(item.rawValue)), terminator: "")
                }
            }
        }
        print()
    }
    return alignmentParam
}

func part1() {

    let (atlas, maxXY) = makeAtlas()

    let alignmentParam = drawAtlas(atlas, maxXY: maxXY)
    print("Alignment Parameter: \(alignmentParam)")

}

func part2() {

    var (atlas, maxXY) = makeAtlas()

    // Find the droid
    var droidPos: CoOrds?
    let droidAliases: [Item] = [.up, .left, .right, .down]
    droidFinder: for posY in 0...maxXY.y {
        for posX in 0...maxXY.x {
            if let found = atlas[CoOrds(x: posX, y: posY)] {
                if droidAliases.contains(found) {
                    // Found the droid
                    droidPos = CoOrds(x: posX, y: posY)
                    print("Droid is in \(droidPos!)")
                    break droidFinder
                }
            }
        }
    }

    guard let startPos = droidPos else {
        print("Did not find the droid!")
        return
    }

    // We now need to traverse the scaffolding Following a visual inspection,
    // I've noticed that the droid is at the very end of the scaffolding so we
    // won't have to loop back on ourselves.  Also, it's possible to traverse
    // the entire scaffolding by going to the end of the section, then turning
    // in the only possible direction that is not going back on yourself.  When
    // you get to a dead end, you've traversed the whole thing.

    // Step 1 - get the full string of instructions

    let expandedSteps = navigateScaffolding(&atlas, from: startPos, maxXY: maxXY)

    print(expandedSteps)
}

enum Direction: Int, CaseIterable {
    // swiftlint:disable identifier_name
    case up = 0
    // swiftlint:enable identifier_name
    case  right, down, left
}

func turnRight(_ from: Direction) -> Direction {
    return Direction(rawValue: (from.rawValue + 1)%Direction.allCases.count)!
}

func turnLeft(_ from: Direction) -> Direction {
    return Direction(rawValue: (Direction.allCases.count + from.rawValue - 1)%Direction.allCases.count)!
}

func droidToDir(_ droidp: Item?) -> Direction? {
    guard let droid = droidp else {
        return nil
    }
    switch droid {
    case .up:
        return .up
    case .right:
        return .right
    case .down:
        return .down
    case .left:
        return .left
    default:
        return nil
    }
}

func dirToDroid(_ dir: Direction) -> Item {
    switch dir {
    case .up:
        return .up
    case .right:
        return .right
    case .down:
        return .down
    case .left:
        return .left
    }
}

func canGoOn(_ atlas: [CoOrds: Item], from startPos: CoOrds, going dir: Direction) -> (CoOrds)? {
    // Returns nil if we can't go on, otherwise returns the next position
    var nextPos = startPos
    switch dir {
    case .up:
        nextPos.y -= 1
    case .down:
        nextPos.y += 1
    case .left:
        nextPos.x -= 1
    case .right:
        nextPos.x += 1
    }
    if atlas[nextPos] != .scaffold {
        return nil
    }
    // We can go on, so we have
    return nextPos
}

func getSegment(_ atlas: inout [CoOrds: Item], from startPos: CoOrds) -> (String, CoOrds)? {
    // Moves from the starting position, and returns the motion string, and
    // where it got to.
    // If there's no way to continue, it returns nil

    // The logic:
    // We can either turn right, or left.  If neither of those directions get
    // us moving again, then we've completed the scaffolding and need to return
    // nil

    // Atlas manipulation:
    // After deciding which way to turn, we replace the droid at our start
    // position to scaffolding.
    // At the end of our run, we change the scaffolding into a droid pointing
    // in our current direction.

    var instruction = ""
    guard let startDir = droidToDir(atlas[startPos]) else {
        print("No droid found! \(atlas[startPos, default: .free])")
        return nil
    }

    var newDir: Direction
    var endPos = startPos

    // Try looking right
    newDir = turnRight(startDir)

    if canGoOn(atlas, from: endPos, going: newDir) != nil {
        instruction = "R"
    } else {
        // Look left instead
        newDir = turnLeft(startDir)
        if canGoOn(atlas, from: endPos, going: newDir) != nil {
        instruction = "L"
        newDir = turnLeft(startDir)
        } else {
        // No way to turn - we're done!
            return nil
        }
    }
    // We know what direction to travel, so go that way
    var count = 0
    while let nextPos = canGoOn(atlas, from: endPos, going: newDir) {
        count += 1
        endPos = nextPos
    }

    // We're at the end now - so tidy up...
    atlas[startPos] = .scaffold
    atlas[endPos] = dirToDroid(newDir)
    instruction += String(count)

    return (instruction, endPos)
}

func navigateScaffolding(_ atlas: inout [CoOrds: Item], from startPos: CoOrds, maxXY: CoOrds) -> String {
    var instructions = ""

    var droidPos = startPos

    while let (addition, newPos) = getSegment(&atlas, from: droidPos) {
        _ = drawAtlas(atlas, maxXY: maxXY)
        droidPos = newPos
        if instructions.count == 0 {
            instructions = addition
        } else {
            instructions += "," + addition
        }
    }
    return instructions
}

func part2b() {
    // I've done this manually in vim

    // B,C,C,A,A,B,C,C,A,B
    // A = L,12,L,8,R,10
    // B = R,4,R,12,R,10,L,12
    // C = L,12,R,4,R,12

    let instructions = "B,C,C,A,A,B,C,C,A,BrL,12,L,8,R,10rR,4,R,12,R,10,L,12rL,12,R,4,R,12ryryryryr"
    // let instructions = "A,B,CrR,4rR,12rR,10ryryryr"

    var input = [Int]()

    for char in instructions {
        if char == "r" {
            input.append(10)
            continue
        }
        if let num = char.asciiValue {
            input.append(Int(num))
        }
    }
    print(input)
    var droidProg = code
    // Switch the droid on
    droidProg[0] = 2
    let droid = IntCode(droidProg, input: input)

    while !droid.halted {
        if let char = droid.run() {
            if char > 255 {
                print("Value: \(char)")
            } else if char == 10 {
                print()
            } else {
                print(Character(UnicodeScalar(char)!), terminator: "")
            }
        }
    }
}

part1()
part2()
part2b()
