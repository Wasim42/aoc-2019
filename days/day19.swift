#!/usr/bin/env swift

import Foundation

// Note that this does not use a queue for input unlike the other days!
// Hence the lack of any regression tests
class IntCode {
    var halted: Bool
    var memory: [Int: Int] = [:]
    var input: [Int]
    var output: Int
    var progCounter: Int
    var relBase: Int
    init(_ code: [Int], input: [Int]? = nil) {
        self.halted = false // We halt when we hit 99
        if let inp = input {
            self.input = inp
        } else {
            self.input = []
        }
        self.progCounter = 0
        self.relBase = 0
        self.output = 0
        // Put the code into memory
        for ind in code.indices {
            self.memory[ind] = code[ind]
        }
    }
    func run(_ input: [Int]? = nil) -> Int? {
        if self.halted {
            print("Halted")
            return nil
        }
        if let passedIn = input {
            self.input += passedIn
        }
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            /*
            var pc = 0
            while true {
                if let value = self.memory[pc] {
                    print("\(pc):\(value)", terminator: " ")
                    pc += 1
                } else {
                    break
                }
            }
            print()
            */
            var codestr = String(self.memory[self.progCounter, default: 0])
            let opstr = codestr.suffix(2)
            let opcode = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.progCounter += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            for i in 0..<3 {
                var paramType = 0
                if codestr.count > 0 {
                    paramType = Int(String(codestr.popLast() ?? "0"))!
                }
                switch paramType {
                case 0:
                    // Positional
                    paramPos.append(self.memory[self.progCounter + i, default: 0])
                case 1:
                    // Immediate
                    paramPos.append(self.progCounter + i)
                case 2:
                    // Relative
                    // ie relBase + what is in this memory location
                    paramPos.append(self.relBase + self.memory[self.progCounter + i, default: 0])
                default:
                    print("Unrecognised parameter type \(paramType)")
                    return nil
                }
            }
            // print(opcode, paramPos)
            switch opcode {
            case 99:
                print("We still have \(self.input.count) inputs to read")
                self.halted = true
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 + in2
                continue
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 * in2
                continue
            case 3:
                // Pass in the current input
                if self.input.count > 0 {
                    self.memory[paramPos[0]] = self.input.removeFirst()
                } else {
                    print("Need more input!")
                    self.memory[paramPos[0]] = 0
                }
                self.progCounter += 1
            case 4:
                // Get output
                self.output = self.memory[paramPos[0], default: 0]
                self.progCounter += 1
                return self.output
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0], default: 0] < self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 8:
                // equals
                if self.memory[paramPos[0], default: 0] == self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 9:
                // Adjust relative base
                self.relBase += self.memory[paramPos[0], default: 0]
                self.progCounter += 1
            default:
                print("Unrecognised opcode \(opcode)")
                return nil
            }
        }
    }
}

func runToCompletion(_ code: [Int], input: [Int]? = nil) -> [Int] {
    var outputs: [Int] = []

    let machine = IntCode(code, input: input)

    while !machine.halted {
        if let output = machine.run() {
            outputs.append(output)
        }
    }
    return outputs
}

var code: [Int] = []
let inputFile = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input")
let input = try String.init(contentsOfFile: inputFile)
.components(separatedBy: "\n")[0]
for char in input.components(separatedBy: ",") {
    if let int = Int(char) {
        code.append(int)
    }
}

enum Motion: Int, CaseIterable {
    case north = 1, east = 4, south = 2, west = 3
}

func opposite(_ motion: Motion) -> Motion {
    switch motion {
    case .north:
        return .south
    case .south:
        return .north
    case .east:
        return .west
    case .west:
        return .east
    }
}

struct CoOrds {
    // swiftlint:disable identifier_name
    var x: Int
    var y: Int
    // swiftlint:enable identifier_name
    mutating func move(_ motion: Motion) {
        switch motion {
        case .north:
            y += 1
        case .south:
            y -= 1
        case .east:
            x += 1
        case .west:
            x -= 1
        }
    }
}

extension CoOrds: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}

func part1() {
    // Get the lay of the tractor beam over the 50x50 square

    var points = 0
    for posY in 0..<50 {
        for posX in 0..<50 {
            let tractor = IntCode(code)
            if tractor.run([posX, posY]) == 1 {
                points += 1
            }
        }
    }
    print("Affected points: \(points)")
}

part1()

func part2() {
    // From a visual inspection, it's obvious that the beam doesn't get a
    // hundred units across until a lot later (at the very least when Y is 100.
    // Also, the start of the beam starts later than the previous line.
    // From the question, we know that for both x, and y, they are at most 4
    // digits
    var firstX = 2
    for posY in 500...9999 {
        print("\n\(posY): ", terminator: "")
        var foundStartX = false
        for posX in firstX-2...9999 {
            var tractor = IntCode(code)
            let inBeam = tractor.run([posX, posY]) == 1
            if !foundStartX {
                if !inBeam {
                    continue
                }
                foundStartX = true
                firstX = posX
            }

            if inBeam {
                // We found a beam.  Let's see whether this 'y' value is still
                // viable by checking whether the beam is visible 100 'x' away
                tractor = IntCode(code)
                if tractor.run([posX+99, posY]) != 1 {
                    // We're finished with this 'y' value
                    break
                }
                print(">", terminator: "")
                // This is still a viable 'y' value... now check the other
                // points of the square
                // If these don't work, it's possible that we just need to move
                // along the 'x' axis
                tractor = IntCode(code)
                if tractor.run([posX, posY+99]) != 1 {
                    // Bottom left-hand corner of the square missing
                    continue
                }
                print("|_", terminator: " ")

                tractor = IntCode(code)
                if tractor.run([posX+99, posY+99]) != 1 {
                    // No bottom-right-hand-corner of the square?
                    // This is unlikely to fail.
                    continue
                }
                // We have our location!
                print("\nNumber to print: \(posX*10000+posY)")
                return
            }
        }
    }
}

part2()
