#!/usr/bin/env swift

import Foundation

func strToInput(string: String) -> [Int] {
    var numbers: [Int] = []
    for char in string {
        if let number = Int(String(char)) {
            numbers.append(number)
        }
    }
    return numbers
}

func outputToStr(output: ArraySlice<Int>) -> String {
    var numbers = ""

    for num in output {
        numbers.append(String(num))
    }
    return numbers
}

func getOffset(from numbers: [Int]) -> Int {
    // We use the first 7 digits

    var offset = 0
    for pos in 0..<7 {
        offset = offset*10 + numbers[pos]
    }
    return offset
}

func applyPhase(input: [Int], pattern: [Int] = [0, 1, 0, -1]) -> [Int] {

    var phasedOutput: [Int] = []

    for pos in 1...input.count {
        // Multiply the pattern by the position
        var multipliedPattern: [Int] = []
        bigCount: while multipliedPattern.count < input.count + 1 {
            // multipliedPattern += pattern.flatMap { Array(repeating: $0, count: pos)}
            for num in pattern {
                let appendix = Array(repeating: num, count: pos)
                multipliedPattern += appendix
                if multipliedPattern.count > input.count {
                    break bigCount
                }
            }
        }
        // Now remove the first element of the multipliedPattern
        multipliedPattern.removeFirst()

        // Now, we loop through the input, multiplying each element by the
        // multipliedPattern, and we will get another digit.
        // Add up all the elements, and get just the unit value
        //let total = abs(zip(input, multipliedPattern).map(*).reduce(0, +)%10)
        var total = 0
        for numPos in 0..<input.count where multipliedPattern[numPos] != 0 {
            total += input[numPos] * multipliedPattern[numPos]
        }
        total = abs(total%10)
        phasedOutput.append(total)
    }

    return phasedOutput
}

// print(outputToStr(output: applyPhase(input: [1, 2, 3, 4, 5, 6, 7, 8])[...]))

func applyTrickPhases(input: ArraySlice<Int>, phases: Int) -> [Int] {

    var latestDigits = Array(input)

    for _ in 1...phases {
        var total = 0
        for pos in (0..<latestDigits.count).reversed() {
            total += latestDigits[pos]
            latestDigits[pos] = total%10
        }
    }
    return latestDigits
}

func part2(input shortInput: String, phases: Int = 100) -> String {

    var fullInput = ""
    for _ in 0..<10000 {
        fullInput += shortInput
    }

    let signal = strToInput(string: fullInput)

    let offset = getOffset(from: signal)

    // The trick:
    // Consider the pattern for a number of repetitions, for each digit:
    // 01: 1  0  -1  0  1  0  -1  0  1
    // 02: 0  1   1  0  0  -1 -1  0  0
    // 03: 0  0   1  1  1  0   0  0  -1
    // 04: 0  0   0  1  1  1   1  0  0
    // 05: 0  0   0  0  1  1   1  1  1
    // 06: 0  0   0  0  0  1   1  1  1
    // 07: 0  0   0  0  0  0   1  1  1
    // 08: 0  0   0  0  0  0   0  1  1
    // 09: 0  0   0  0  0  0   0  0  1
    //
    // Notice that after a transform, the last digit is always the same,
    // The 2nd-to-last digit is the sum of the last two digits (mod 10)
    // The 3rd-to-last digit is the sum of the last three digits (mod 10)
    //
    // We have a simple technique which applies to only the last half of
    // the digits.
    //
    // At phase 2:
    // The last digit always remains the same
    // Penultimate digit 'x':
    //              Phase 1                   Phase 2
    //  x = sum(last two digits)%10 + (sum(last two digits)%10 + last)%10
    //

    precondition(offset > signal.count/2, "The offset is too small")

    let significant = signal[offset...]
    return outputToStr(output: applyTrickPhases(input: significant, phases: phases)[...7])

}

func applyManyPhases(input: [Int], count: Int, pattern: [Int] = [0, 1, 0, -1]) -> [Int] {
    var lastOne = input
    for _ in 0..<count {
        lastOne = applyPhase(input: lastOne, pattern: pattern)
    }
    return lastOne
}

struct TestEntry {
    var input: String
    var count: Int
    var answer: String
}

var part1Tests: [TestEntry] = []

part1Tests.append(TestEntry(input: "12345678",
                            count: 4,
                            answer: "01029498"))
part1Tests.append(TestEntry(input: "80871224585914546619083218645595",
                            count: 100,
                            answer: "24176176"))
part1Tests.append(TestEntry(input: "19617804207202209144916044189917",
                            count: 100,
                            answer: "73745418"))
part1Tests.append(TestEntry(input: "69317163492948606335995924319873",
                            count: 100,
                            answer: "52432133"))
// Apply a number of phases to an input, and return the first 8 numbers of the
// full answer
func part1(input: String, phases: Int = 100) -> String {
    let numbers = strToInput(string: input)

    let guess = applyManyPhases(input: numbers, count: phases)[0..<8]
    return outputToStr(output: guess)
}

var part2Tests: [TestEntry] = []

part2Tests.append(TestEntry(input: "03036732577212944063491565474664",
                            count: 100,
                            answer: "84462026"
                            ))
part2Tests.append(TestEntry(input: "02935109699940807407585447034323",
                            count: 100,
                            answer: "78725270"
                            ))
part2Tests.append(TestEntry(input: "03081770884921959731165446850517",
                            count: 100,
                            answer: "53553731"
                            ))

var passes = 0
for test in part1Tests {
    let guess = part1(input: test.input, phases: test.count)
    if guess == test.answer {
        passes += 1
        print("\(passes) passes")
    } else {
        print("\(guess) != \(test.answer) for \(test.input)")
    }
}

if passes == part1Tests.count {
    print("100% pass!")
    if CommandLine.argc == 2 {
        let input = try String.init(contentsOfFile: CommandLine.arguments[1])
        print("Part1: \(part1(input: input, phases: 100))")
    }
}

passes = 0
for test in part2Tests {
    let guess = part2(input: test.input, phases: test.count)
    if guess == test.answer {
        passes += 1
        print("Passed \(passes)")
    } else {
        print("\(guess) != '(test.answer) for \(test.input)'")
    }
}

if passes == part2Tests.count {
    print("100% pass!")
    if CommandLine.argc == 2 {
        let input = try String.init(contentsOfFile: CommandLine.arguments[1])
        print("Part2: \(part2(input: input, phases: 100))")
    }
}
