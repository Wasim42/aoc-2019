#!/usr/bin/env swift

import Foundation

struct CoOrds {
    var x: Int
    var y: Int
}

extension CoOrds: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}

func day3() -> [Int]? {
    do {
        var numbers: [String]

        if CommandLine.argc > 2 ||
        CommandLine.argc > 1 && CommandLine.arguments[1].hasPrefix("-") {
            // The equivalent of --help
            print("\(CommandLine.arguments[0]) [filename]")
            print("\n   If a filename is not provided, we read from stdin")
            return nil
        } else if CommandLine.argc == 2 {
            // We have been given the filename to use
            let nums: [String] = try String.init(contentsOfFile: CommandLine.arguments[1])
            .components(separatedBy: "\n")
            numbers = nums
        } else {
            // Read from stdin continually until no more input.
            print("Please enter your input - blank line to finish:")
            numbers = []
            while true {
                if let line = readLine() {
                    if line.count < 1 {
                        // Blank line - finish
                        break
                    }
                    numbers.append(line)
                } else {
                    // ctrl-d - finish
                    break
                }
            }
        }

        // The input is two lines, comma-separated

        // First one first - we store the locations that this one hits in a
        // dictionary to allow us reasonable look-ups later
        var circuit = [CoOrds: Int]()
        var directions = numbers[0].components(separatedBy: ",")

        var curPos = CoOrds(x: 0, y: 0)
        var steps = 0

        for d in directions {
            // d will be of the form Xy* where X is the direction,
            // and y* is an integer
            let dir = String(d.prefix(1))

            let samount = d.suffix(from: d.index(d.startIndex, offsetBy: 1))
            guard let amount = Int(samount) else {
                print("Bad amount \(samount)")
                return nil
            }

            for _ in 1...amount {
                switch dir {
                case "D":
                    curPos.y -= 1
                case "U":
                    curPos.y += 1
                case "L":
                    curPos.x -= 1
                case "R":
                    curPos.x += 1
                default:
                    print("Unknown direction \(dir) in \(d)")
                    return nil
                }
                // Now record this position...
                // Watch out for repetitions - the instructions imply that's
                // possible
                steps += 1
                if circuit[curPos] == nil {
                    circuit[curPos] = steps
                }
            }
        }

        // Now find the crossing points by doing almost the same for the next
        // circuit
        var crossings: [CoOrds:Int] = [:]

        directions = numbers[1].components(separatedBy: ",")
        curPos.x = 0
        curPos.y = 0
        steps = 0

        for d in directions {
            // d will be of the form Xy* where X is the direction,
            // and y* is an integer
            let dir = String(d.prefix(1))

            let samount = d.suffix(from: d.index(d.startIndex, offsetBy: 1))
            guard let amount = Int(samount) else {
                print("Bad amount \(samount)")
                return nil
            }

            for _ in 1...amount {
                switch dir {
                case "D":
                    curPos.y -= 1
                case "U":
                    curPos.y += 1
                case "L":
                    curPos.x -= 1
                case "R":
                    curPos.x += 1
                default:
                    print("Unknown direction \(dir) in \(d)")
                }
                steps += 1
                if let othersteps = circuit[curPos] {
                    // Found a match
                    let totalsteps = steps + othersteps
                    // We might have hit this crossing before...
                    if let foundsteps = crossings[curPos] {
                        if totalsteps < foundsteps {
                            crossings[curPos] = totalsteps
                        }
                    } else {
                        crossings[curPos] = totalsteps
                    }
                }
            }
        }

        print("Crossings:", crossings)

        // Now calculate the nearest to the origin point

        var nearest: UInt?
        var fewestSteps: Int?

        for (pos, steps) in crossings {
            let distance = pos.x.magnitude + pos.y.magnitude
            if nearest == nil || distance < nearest! {
                nearest = distance
            }
            if fewestSteps == nil || steps < fewestSteps! {
                fewestSteps = steps
            }
        }
        return [Int(nearest!), fewestSteps!]

    } catch {
        print("Sorry - I failed to read \(CommandLine.arguments[1])!")
    }
    return nil
}

if let answer = day3() {
    print(answer)
}
