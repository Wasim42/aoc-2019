#!/usr/bin/env swift

import Foundation

let input = """
#..##
#.#..
#...#
##..#
#..##
"""

let exampleStart = """
....#
#..#.
#..##
..#..
#....
"""

let exampleAfter4Rating = 2129920

class Eris: CustomStringConvertible {
    var bugLand: [Bool]

    var previous: Set<[Bool]>

    var description: String {
        var msg = ""
        for count in 0..<bugLand.count {
            if count > 0 && count%5 == 0 {
                msg += "\n"
            }
            msg += (bugLand[count] ? "#" : ".")
        }
        return msg
    }
    init(_ input: String) {
        bugLand = []
        previous = []
        for bug in input {
            switch bug {
            case ".":
                bugLand.append(false)
            case "#":
                bugLand.append(true)
            default:
                // The other characters are just filler
                break
            }
        }
    }
    func above(_ num: Int) -> Bool {
        let over = num - 5
        if over < 0 {
            return false
        }
        return bugLand[over]
    }
    func below(_ num: Int) -> Bool {
        let under = num + 5
        if under >= bugLand.count {
            return false
        }
        return bugLand[under]
    }
    func left(_ num: Int) -> Bool {
        let gauche = num - 1
        if (5 + gauche)%5 == 4 {
            return false
        }
        return bugLand[gauche]
    }
    func right(_ num: Int) -> Bool {
        let droit = num + 1
        if (droit)%5 == 0 {
            return false
        }
        return bugLand[droit]
    }
    func neighbourCount(_ square: Int) -> Int {
        var count = 0
        let checks = [self.left, self.right, self.above, self.below]
        for check in checks {
            if check(square) {
                count += 1
            }
        }
        return count
    }
    func run() -> Bool {
        // Returns true if this run did not result in a repeat
        var newLand: [Bool] = []
        for square in 0..<bugLand.count {
            let bug = bugLand[square]
            if bug {
                // A bug dies (becoming an empty space) unless there is exactly
                // one bug adjacent to it.
                if neighbourCount(square) != 1 {
                    newLand.append(false)
                } else {
                    newLand.append(true)
                }
            } else {
                // An empty space becomes infested with a bug if exactly one or
                // two bugs are adjacent to it.
                let count = neighbourCount(square)
                if count == 1 || count == 2 {
                    newLand.append(true)
                } else {
                    newLand.append(false)
                }
            }
        }
        bugLand = newLand
        if previous.contains(bugLand) {
            return false
        }
        previous.insert(bugLand)
        return true
    }
    var bioRating: Int {
        var rating = 0
        for tile in 0..<bugLand.count where bugLand[tile] {
            // pow() returns a Decimal, and it's not straightforward to convert
            // it to an int!
            rating += Int(pow(2, tile).description)!
        }
        return rating
    }
}

let exam = Eris(exampleStart)

while exam.run() {
}
if exam.bioRating == exampleAfter4Rating {
    print("Passed test!")
}

let part1 = Eris(input)

while part1.run() {
}
print("Part 1: \(part1.bioRating)")
