#!/usr/bin/env swift

import Foundation

struct TestData {
    var input: String
    var answer: [Int] // x, y, numAsteroids
}

var examples: [TestData] = []

examples.append(TestData(input: """
                         .#..#
                         .....
                         #####
                         ....#
                         ...##
                         """, answer: [3, 4, 8]))
examples.append(TestData(input: """
                         ......#.#.
                         #..#.#....
                         ..#######.
                         .#.#.###..
                         .#..#.....
                         ..#....#.#
                         #..#....#.
                         .##.#..###
                         ##...#..#.
                         .#....####
                         """, answer: [5,8,33]))
examples.append(TestData(input: """
                         #.#...#.#.
                         .###....#.
                         .#....#...
                         ##.#.#.#.#
                         ....#.#.#.
                         .##..###.#
                         ..#...##..
                         ..##....##
                         ......#...
                         .####.###.
                         """, answer: [1,2,35]))
examples.append(TestData(input: """
                         .#..#..###
                         ####.###.#
                         ....###.#.
                         ..###.##.#
                         ##.##.#.#.
                         ....###..#
                         ..#.#..#.#
                         #..#.#.###
                         .##...##.#
                         .....#.#..
                         """, answer: [6,3,41]))
examples.append(TestData(input: """
                         .#..##.###...#######
                         ##.############..##.
                         .#.######.########.#
                         .###.#######.####.#.
                         #####.##.#.##.###.##
                         ..#####..#.#########
                         ####################
                         #.####....###.#.#.##
                         ##.#################
                         #####.##.###..####..
                         ..######..##.#######
                         ####.##.####...##..#
                         .#####..#.######.###
                         ##...#.##########...
                         #.##########.#######
                         .####.#.###.###.#.##
                         ....##.##.###..#####
                         .#.#.###########.###
                         #.#.#.#####.####.###
                         ###.##.####.##.#..##
                         """, answer: [11,13,210,35]))

struct CoOrds {
    var x: Int
    var y: Int
}

extension CoOrds: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}

func gcd(_ inA: Int, _ inB: Int) -> Int {

    var a = inA
    var b = inB

    while b != 0 {
        let t = b
        b = a % b
        a = t
    }
    return a
}

func astsInLineOfSight(from: CoOrds, via: CoOrds, width: Int, height: Int) -> [CoOrds] {
    // from: The potential site of a station
    // via: The position of another asteroid
    // width, height: The size of the asteroid field
    // returns: The co-ordinates which are blocked by this asteroid

    var blocked: [CoOrds] = []

    if from == via {
        // Check for the error condition
        return blocked
    }

    // The way this works is that we have a straight line from "from" to "via"
    // We get an increment which lies on the straight line.
    // We keep extending that line beyond via until we reach a boundary, adding
    // those locations to our blocked list.

    // To determine the increments, we need to find the right ratio.
    var incX: Int
    var incY: Int

    // First, if either the x, or the y value of both from, and via are the
    // same, then we increment the other value by 1 (or -1)
    if from.x == via.x {
        // On the same horizontal line
        incX = 0
        if from.y > via.y {
            incY = -1
        } else {
            incY = 1
        }
    } else if from.y == via.y {
        // On the same vertical line
        incY = 0
        if from.x > via.x {
            incX = -1
        } else {
            incX = 1
        }
    } else {
        // Ratio time!
        incX = via.x - from.x
        incY = via.y - from.y

        // Watch out for negative common divisors!
        let divisor = Int(gcd(incX, incY).magnitude)

        incX = incX / divisor
        incY = incY / divisor
    }


    var position = CoOrds(x: via.x, y: via.y)
    while true {
        position.x += incX
        position.y += incY
        if position.x < 0 || position.x > width {
            break
        }
        if position.y < 0 || position.y > height {
            break
        }
        blocked.append(position)
    }
    return blocked
}

class AsteroidField: CustomStringConvertible {
    var asteroids: Set<CoOrds>
    var width: Int
    var height: Int
    var description: String {
        var d = ""
        for y in 0...height {
            var line = ""
            for x in 0...width {
                if asteroids.contains(CoOrds(x:x, y:y)) {
                    line.append("#")
                } else {
                    line.append(".")
                }
            }
            line.append("\n")
            d.append(line)
        }
        return d
    }

    init(_ input: String) {
        var y = 0
        var x = 0
        width = 0
        height = 0
        asteroids = []
        for line in input.components(separatedBy: "\n") {
            if line.count < 1 {
                // An invalid line, just skip
                continue
            }
            x = 0
            line.forEach {
                if $0 == "#" {
                    asteroids.insert(CoOrds(x: x, y: y))
                }
                width = x
                x += 1
            }
            height = y
            y += 1
        }
    }

    func getVisibleAsteroids(from me: CoOrds) -> Set<CoOrds> {

        // Get a local copy of the asteroid field so we can manipulate it as
        // required.
        var localAsteroids = self.asteroids
        // Remove ourself
        localAsteroids.remove(me)

        // The blocked list that's returned will return the list of locations
        // that are blocked, but not including the asteroid that's passed in.
        // This means that we can pass in all the known asteroids, and if we
        // remove them, we'll be left with only the visible ones.

        for a in localAsteroids {
            let targets = astsInLineOfSight(from: me, via: a, width: self.width, height: self.height)
            for t in targets {
                localAsteroids.remove(t)
            }
        }
        return localAsteroids
    }

    func bestLocation() -> (loc: CoOrds, hits: Int) {

        var allHits: [CoOrds:Int] = [:]
        for loc in asteroids {
            let hits = self.getVisibleAsteroids(from: loc).count
            allHits[loc] = hits
        }

        var bestLoc = CoOrds(x: 0, y: 0)
        var bestHits = 0
        for (loc, hits) in allHits {
            if hits > bestHits {
                bestLoc = loc
                bestHits = hits
            }
        }
        return (loc: bestLoc, hits: bestHits)
    }

    func getHit(number: Int, from station: CoOrds) {
        // This starts the shooting session...
        var hits = 0
        while true {
            // The first thing we do in each round is get the list of asteroids
            // in the line of sight.
            let visibles = self.getVisibleAsteroids(from: station)

            var asteroidsByAngle: [Double:CoOrds] = [:]
            for a in visibles {
                // atan2 converts from cartesian to polar co-ordinates
                // Unfortunately the instructions we have require us to use a
                // clockwise rotation, starting at 12 o'clock
                // atan2 starts at 3 o'clock and rotates anti-clockwise.
                // Also our 'y' values are backwards.  Up is negative y, but
                // right is positive x
                // atan2 assumes up is positive y, and right is positive x.
                //
                // To get this right, we'll flip the y values, and subtract the
                // angle from pi/2
                var angle = Double.pi/2 - atan2(Double(station.y - a.y),
                                              Double(a.x - station.x))
                if angle < 0 {
                    angle += Double.pi * 2
                }
                asteroidsByAngle[angle] = a
            }
            // We now have a dictionary of visible asteroids, by angle.  To do
            // the laser them away, we need to sort the asteroids in numerical
            // order, and burn them away from the asteroid field itself
            for angle in asteroidsByAngle.keys.sorted() {
                if let asteroid = asteroidsByAngle[angle] {
                    self.asteroids.remove(asteroid)
                    hits += 1
                    if hits == 200 {
                        print("Asteroid location \(asteroid)")
                        return
                    }
                }
            }
        }
    }
}

var passes = 0
for t in examples {
    let af = AsteroidField(t.input)
    let guess = af.bestLocation()
    if guess.loc.x == t.answer[0] && guess.loc.y == t.answer[1] && guess.hits == t.answer[2] {
        passes += 1
        print("Passed")
    } else {
        print("Failed: Guessed \(guess) for \(t.answer) in\n\(af)")
    }
}

if passes == examples.count {
    print("100% pass")
    if CommandLine.argc == 2 {
        let input = try String.init(contentsOfFile: CommandLine.arguments[1])
        let af = AsteroidField(input)
        let station = af.bestLocation()
        print(station)

        // Now we need to blow them away, and just print out the location of
        // the 200th one
        af.getHit(number: 200, from: station.loc)
    }
}
