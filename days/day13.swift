#!/usr/bin/env swift

import Foundation

// Note that this does not use a queue for input unlike the other days!
// Hence the lack of any regression tests
class IntCode {
    var halted: Bool
    var memory: [Int:Int] = [:]
    var input: Int
    var output: Int
    var pc: Int
    var relBase: Int
    init(code: [Int], input: Int? = nil) {
        self.halted = false // We halt when we hit 99
        self.input = 0
        if let i = input {
            self.input = i
        }
        self.pc = 0
        self.relBase = 0
        self.output = 0
        // Put the code into memory
        for i in code.indices {
            self.memory[i] = code[i]
        }
    }
    func run(_ input: Int? = nil) -> Int? {
        if self.halted {
            print("Halted")
            return nil
        }
        if let passedIn = input {
            self.input = passedIn
        }
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            /*
            var pc = 0
            while true {
                if let value = self.memory[pc] {
                    print("\(pc):\(value)", terminator: " ")
                    pc += 1
                } else {
                    break
                }
            }
            print()
            */
            var codestr = String(self.memory[self.pc, default: 0])
            let opstr = codestr.suffix(2)
            let op = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.pc += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            for i in 0..<3 {
                var paramType = 0
                if codestr.count > 0 {
                    paramType = Int(String(codestr.popLast() ?? "0"))!
                }
                switch paramType {
                case 0:
                    // Positional
                    paramPos.append(self.memory[self.pc + i, default: 0])
                case 1:
                    // Immediate
                    paramPos.append(self.pc + i)
                case 2:
                    // Relative
                    // ie relBase + what is in this memory location
                    paramPos.append(self.relBase + self.memory[self.pc + i, default: 0])
                default:
                    print("Unrecognised parameter type \(paramType)")
                    return nil
                }
            }
            // print(op, paramPos)
            switch op {
            case 99:
                self.halted = true
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.pc += 3
                self.memory[outpos] = in1 + in2
                continue
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.pc += 3
                self.memory[outpos] = in1 * in2
                continue
            case 3:
                // Pass in the current input
                self.memory[paramPos[0]] = self.input
                self.pc += 1
            case 4:
                // Get output
                self.output = self.memory[paramPos[0], default: 0]
                self.pc += 1
                return self.output
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.pc = self.memory[paramPos[1], default: 0]
                } else {
                    self.pc += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.pc = self.memory[paramPos[1], default: 0]
                } else {
                    self.pc += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0], default: 0] < self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.pc += 3
            case 8:
                // equals
                if self.memory[paramPos[0], default: 0] == self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.pc += 3
            case 9:
                // Adjust relative base
                self.relBase += self.memory[paramPos[0], default: 0]
                self.pc += 1
            default:
                print("Unrecognised opcode \(op)")
                return nil
            }
        }
    }
}

var code: [Int] = []
if CommandLine.argc == 2 {
    let input = try String.init(contentsOfFile: CommandLine.arguments[1])
    .components(separatedBy: "\n")[0]
    for n in input.components(separatedBy: ",") {
        if let i = Int(n) {
            code.append(i)
        }
    }
}

func runToCompletion(code: [Int], input: Int = 0) -> [Int]{
    var outputs: [Int] = []

    let machine = IntCode(code: code, input: input)

    while !machine.halted {
        if let output = machine.run(input) {
            outputs.append(output)
        }
    }
    return outputs
}

struct CoOrds {
    var x: Int
    var y: Int
}

extension CoOrds: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}


/*
func runRobot(code: [Int]) -> Set<CoOrds> {
    // The first panel is always zero
    var robot = IntCode(code: code, input: 0)
}
*/

func day13pt1() {
    var output = runToCompletion(code: code)
    // Parse the output
    var blocks: [CoOrds:Int] = [:]
    while true {
        if output.count < 3 {
            break
        }
        blocks[CoOrds(x: output.removeFirst(), y: output.removeFirst())] = output.removeFirst()
    }
    // How many block tiles are on the screen when the game exits?
    var blockCount = 0
    for (_, i) in blocks {
        if i == 2 {
            blockCount += 1
        }
    }
    print("There are \(blockCount) tiles")
}

day13pt1()

func day13pt2() {
    // A game-playing engine.
    // Step 1 - put in the fake coins
    code[0] = 2

    // Strategy - Use the joystick to move the paddle so that it's always under
    // the ball.
    // Keep track of the score
    // Keep playing until there are no more blocks reported?  Or until the game halts?

    var score = 0
    var paddleC: CoOrds?
    var blocks: Set<CoOrds> = []

    // Assumptions:
    // For the purpose of this task, we can ignore the walls, and the empty
    // blocks.  They are only for those who want to do some fancy graphics that
    // can be shown off, and turned into a game in a presentation in the future

    // If the ball has the same CoOrds as a block, then we delete that block.

    // Still not sure when the joystick input is read.

    let breakOut = IntCode(code: code)
    var input = 0
    while true {
        if breakOut.halted {
            break
        }
        var pos: CoOrds
        if let x = breakOut.run(input) {
            if let y = breakOut.run(input) {
                pos = CoOrds(x: x, y: y)
            } else {
                break
            }
        } else {
            break
        }

        if pos.x == -1 && pos.y == 0 {
            // The score!
            score = breakOut.run(input)!
            continue
        }
        let item = breakOut.run(input)!

        switch item {
        case 0, 1:
            // We're ignoring 0 (empty tile) and 1 (wall)
            continue
        case 2:
            // Block tile
            blocks.insert(pos)
        case 3:
            // Paddle tile
            paddleC = pos
        case 4:
            // Ball
            // Adjust the position of the paddle if possible
            if let paddle = paddleC {
                if paddle.x < pos.x {
                    input = 1
                } else if paddle.x > pos.x {
                    input = -1
                } else {
                    input = 0
                }
            }
            // Remove any blocks in the location of the ball
            blocks.remove(pos)
            if blocks.count == 0 {
                break
            }
        default:
            print("Unrecognised item \(item)")
            return
        }
    }
    print("Score: \(score)")

}

day13pt2()
