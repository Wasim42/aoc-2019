#!/usr/bin/env swift

import Foundation

// Note that this does not use a queue for input unlike the other days!
// Hence the lack of any regression tests
class IntCode {
    var halted: Bool
    var memory: [Int: Int] = [:]
    var input: Int
    var output: Int
    var progCounter: Int
    var relBase: Int
    init(_ code: [Int], input: Int? = nil) {
        self.halted = false // We halt when we hit 99
        self.input = 0
        if let inp = input {
            self.input = inp
        }
        self.progCounter = 0
        self.relBase = 0
        self.output = 0
        // Put the code into memory
        for ind in code.indices {
            self.memory[ind] = code[ind]
        }
    }
    func run(_ input: Int? = nil) -> Int? {
        if self.halted {
            print("Halted")
            return nil
        }
        if let passedIn = input {
            self.input = passedIn
        }
        while true {
            // The first number is an opcode, and potentially a set of operating modes
            /*
            var pc = 0
            while true {
                if let value = self.memory[pc] {
                    print("\(pc):\(value)", terminator: " ")
                    pc += 1
                } else {
                    break
                }
            }
            print()
            */
            var codestr = String(self.memory[self.progCounter, default: 0])
            let opstr = codestr.suffix(2)
            let opcode = Int(opstr) ?? 0
            // Now remove the opcode
            codestr.removeLast()
            // It's possible that it's a single-digit opcode...
            if codestr.count > 0 {
                codestr.removeLast()
            }
            // WARNING - we have incremented the program counter
            self.progCounter += 1
            // We have potentially three parameters.  They are either positional,
            // or immediate parameters.
            var paramPos: [Int] = []
            for i in 0..<3 {
                var paramType = 0
                if codestr.count > 0 {
                    paramType = Int(String(codestr.popLast() ?? "0"))!
                }
                switch paramType {
                case 0:
                    // Positional
                    paramPos.append(self.memory[self.progCounter + i, default: 0])
                case 1:
                    // Immediate
                    paramPos.append(self.progCounter + i)
                case 2:
                    // Relative
                    // ie relBase + what is in this memory location
                    paramPos.append(self.relBase + self.memory[self.progCounter + i, default: 0])
                default:
                    print("Unrecognised parameter type \(paramType)")
                    return nil
                }
            }
            // print(opcode, paramPos)
            switch opcode {
            case 99:
                self.halted = true
                return nil
            case 1:
                // Add
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 + in2
                continue
            case 2:
                // Multiply
                let in1 = self.memory[paramPos[0], default: 0]
                let in2 = self.memory[paramPos[1], default: 0]
                let outpos = paramPos[2]
                self.progCounter += 3
                self.memory[outpos] = in1 * in2
                continue
            case 3:
                // Pass in the current input
                self.memory[paramPos[0]] = self.input
                self.progCounter += 1
            case 4:
                // Get output
                self.output = self.memory[paramPos[0], default: 0]
                self.progCounter += 1
                return self.output
            case 5:
                // jump-if-true
                if self.memory[paramPos[0]] != 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 6:
                // jump-if-false
                if self.memory[paramPos[0]] == 0 {
                    self.progCounter = self.memory[paramPos[1], default: 0]
                } else {
                    self.progCounter += 2
                }
            case 7:
                // less than
                if self.memory[paramPos[0], default: 0] < self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 8:
                // equals
                if self.memory[paramPos[0], default: 0] == self.memory[paramPos[1], default: 0] {
                    self.memory[paramPos[2]] = 1
                } else {
                    self.memory[paramPos[2]] = 0
                }
                self.progCounter += 3
            case 9:
                // Adjust relative base
                self.relBase += self.memory[paramPos[0], default: 0]
                self.progCounter += 1
            default:
                print("Unrecognised opcode \(opcode)")
                return nil
            }
        }
    }
}

var code: [Int] = []
if CommandLine.argc == 2 {
    let input = try String.init(contentsOfFile: CommandLine.arguments[1])
    .components(separatedBy: "\n")[0]
    for n in input.components(separatedBy: ",") {
        if let i = Int(n) {
            code.append(i)
        }
    }
}

enum Motion: Int, CaseIterable {
    case north = 1, east = 4, south = 2, west = 3
}

func opposite(_ motion: Motion) -> Motion {
    switch motion {
    case .north:
        return .south
    case .south:
        return .north
    case .east:
        return .west
    case .west:
        return .east
    }
}

struct CoOrds {
    // swiftlint:disable identifier_name
    var x: Int
    var y: Int
    // swiftlint:enable identifier_name
    mutating func move(_ motion: Motion) {
        switch motion {
        case .north:
            y += 1
        case .south:
            y -= 1
        case .east:
            x += 1
        case .west:
            x -= 1
        }
    }
}

extension CoOrds: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}

enum Items: Int {
    case unknown = 0, free, oxygen, wall
}

func bfs(_ atlas: [CoOrds: Items]) -> Int {
    // Given an atlas, return the number of moves required to get to the oxygen

    var visited: [CoOrds: Bool] = [:]

    var queue: [(CoOrds, Int)] = [] // CoOrdinates, and the number of moves

    var position = CoOrds(x: 0, y: 0)
    visited[position] = true
    queue.append((position, 0))

    while queue.count > 0 {

        let (quizPos, depth) = queue.removeFirst()
        position = quizPos

        // Examine all possible locations from here
        for dir in Motion.allCases {
            var newPos = position
            newPos.move(dir)

            if atlas[newPos] == .oxygen {
                // We found it!
                return depth + 1
            }
            if visited[newPos] == true {
                continue
            }
            if atlas[newPos] == .wall {
                continue
            }
            queue.append((newPos, depth+1))
            visited[newPos] = true
        }
    }
    return -1
}

func bfs_oxygenate(_ inAtlas: [CoOrds: Items], origin: CoOrds) -> Int {
    // Given an atlas, retuns the number of minutes required to oxygenate it all
    // Origin is where we start the oxygenation process

    var atlas = inAtlas

    var queue: [(CoOrds, Int)] = [] // CoOrdinates, and the number of minutes

    queue.append((origin, 0))

    var totalMinutes = 0
    while queue.count > 0 {

        let (position, depth) = queue.removeFirst()

        if depth > totalMinutes {
            totalMinutes = depth
        }

        // Examine all possible locations from here
        for dir in Motion.allCases {
            var newPos = position
            newPos.move(dir)

            if atlas[newPos] == .oxygen {
                // Already oxygenated!
                continue
            }
            if atlas[newPos] == .wall {
                // Boing!
                continue
            }
            queue.append((newPos, depth+1))
            atlas[newPos] = .oxygen
        }
    }
    return totalMinutes
}

func part1() {

    var position = CoOrds(x: 0, y: 0)

    var atlas: [CoOrds: Items] = [position: .free]

    var oxygenPos: CoOrds?

    let droid = IntCode(code)

    // Required for drawing the world only...
    var maxX = 0, maxY = 0, minX = 0, minY = 0

    // First, let's explore the whole world
    func examine() {

        for dir in Motion.allCases {
            var newPos = position
            newPos.move(dir)
            if atlas[newPos] != nil {
                // Already seen it
                continue
            }
            maxX = newPos.x > maxX ? newPos.x : maxX
            maxY = newPos.y > maxY ? newPos.y : maxY
            minX = newPos.x < minX ? newPos.x : minX
            minY = newPos.y < minY ? newPos.y : minY

            // Make the move
            let status = droid.run(dir.rawValue)!
            if status == 0 {
                // Hit a wall - our position hasn't changed
                atlas[newPos] = .wall
                continue
            }
            // Move it!
            position = newPos
            if status == 2 {
                oxygenPos = position
                atlas[position] = .oxygen
            } else {
                atlas[position] = .free
            }
            examine()
            // Now go back, but only if we did move
            if status > 0 && droid.run(opposite(dir).rawValue) != 1 {
                // Something odd has happened
                print("Unable to go \(opposite(dir))!")
            } else {
                position.move(opposite(dir))
            }
        }
    }

    examine()

    drawAtlas(atlas, bottomLeft: CoOrds(x: minX, y: minY), topRight: CoOrds(x: maxX, y: maxY))

    if let oxygenLoc = oxygenPos {
        print("Part 1: Number of moves: \(bfs(atlas))")
        print("Part 2: Total minutes to oxygenate: \(bfs_oxygenate(atlas, origin: oxygenLoc))")
    } else {
        print("No oxygen found - you will die!")
    }

}

func drawAtlas(_ atlas: [CoOrds: Items], bottomLeft: CoOrds, topRight: CoOrds) {
    // Draw what we have found
    var oxygenX, oxygenY: Int?
    for posY in (bottomLeft.y...topRight.y).reversed() {
        for posX in bottomLeft.x...topRight.x {
            var char = ""
            switch atlas[CoOrds(x: posX, y: posY), default: .unknown] {
            case .unknown:
                char = " "
            case .free:
                char = "."
            case .oxygen:
                char = "O"
                oxygenX = posX
                oxygenY = posY
            case .wall:
                char = "#"
            }
            if posX == 0 && posY == 0 {
                // Origin
                char = "+"
            }
            print(char, terminator: "")
        }
        if posY == oxygenY {
            print("   <--- Oxygen")
        } else {
            print()
        }
    }
    if oxygenX != nil {
        for _ in bottomLeft.x..<oxygenX! {
            print(" ", terminator: "")
        }
        print("^----- Oxygen")
    }
}

part1()
