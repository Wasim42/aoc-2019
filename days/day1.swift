#!/usr/bin/env swift

// This is a program which reads a bunch of numbers, and adds them up.

import Foundation

func getFuel(for mass: Int) -> Int {
    return mass / 3 - 2
}

let inputFile = CommandLine.arguments[0].replacingOccurrences(of: "swift", with: "input")
let numbers = try String.init(contentsOfFile: inputFile)
.components(separatedBy: "\n")

func day1() -> [Int] {
    var part1 = 0
    var part2 = 0

    for str in numbers {
        guard let num = Int(str) else {
            continue
        }
        let fuel = getFuel(for: num)
        part1 += fuel
        var extraFuel = fuel
        while extraFuel > 0 {
            part2 += extraFuel
            extraFuel = getFuel(for: extraFuel)
        }
    }

    return [part1, part2]
}

let answer = day1()
print("Part1: \(answer[0])")
print("Part2: \(answer[1])")
